<?php
include_once ("../assets/fpdf/fpdf.php");
require '../conexion.php';
class PDF extends FPDF
{
// Cabecera de página
    function Header()
    {
        // Logo
        $this->Image('../assets/inicio.PNG',10,8,20);
        // Arial bold 15
        $this->SetFont('Arial','B',15);
        // Movernos a la derecha
        $this->Cell(80);
        // Título
        $this->Cell(30,10,utf8_decode('Reporte de Área'),0,0,'C');
        // Salto de línea
        $this->Ln(1);

        $this->SetY(28);
    }

    // Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
}
//hORIWNTACION L=Horizontal
$pdf=new PDF();
$pdf->AliasNbPages();
//Primera página
$pdf->AddPage();
$query=$conexion->prepare("SELECT * FROM area WHERE estado = 'A' ");
$query->execute();
$personal = $query->fetchAll(PDO::FETCH_ASSOC);

$pdf->SetFont('Arial','B',12);
$pdf->Cell(20,5,"Item",1,0,'C');
$pdf->Cell(165,5,utf8_decode("Área"),1,1,'C');
$pdf->SetFont('Arial','',10);
$i=1;
foreach ($personal as $value){
    $pdf->Cell(20,5,str_pad($i,2,0,STR_PAD_LEFT),1,0,'C');
    $pdf->Cell(165,5,utf8_decode($value['descripcion']),1,1,'L');
    $i++;
}
$pdf->Output();
?>