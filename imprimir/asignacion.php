<?php
include_once ("../assets/fpdf/fpdf.php");
require '../conexion.php';
class PDF extends FPDF
{
// Cabecera de página
    function Header()
    {
        // Logo
        $this->Image('../assets/inicio.PNG',10,8,20);
        // Arial bold 15
        $this->SetFont('Arial','B',15);
        // Movernos a la derecha
        $this->Cell(80);
        // Título
        $this->Cell(30,10,utf8_decode('Reporte de Asignación de Equipo'),0,0,'C');
        // Salto de línea
        $this->Ln(1);
    }

    // Pie de página
    function Footer()
    {
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
}
//hORIWNTACION L=Horizontal
$pdf=new PDF();
$pdf->AliasNbPages();
//Primera página
$pdf->AddPage();
$pdf->SetY(30);
$query=$conexion->prepare("SELECT per.*,eq.* FROM equipo_persona ep 
                          INNER JOIN persona per ON (ep.id_persona=per.id_persona)
                          INNER JOIN equipo eq ON (ep.id_equipo=eq.id_equipo)");
$query->execute();

$asignacion = $query->fetchAll(PDO::FETCH_ASSOC);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(10,5,"Item",1,0,'C');
$pdf->Cell(75,5,"Responsable",1,0,'C');
$pdf->Cell(35,5,"Marca",1,0,'C');
$pdf->Cell(35,5,"Modelo",1,0,'C');
$pdf->Cell(30,5,"IP Equipo",1,1,'C');
$pdf->SetFont('Arial','',10);
$i=1;
foreach ($asignacion as $value){
    $pdf->Cell(10,5,str_pad($i,2,0,STR_PAD_LEFT),1,0,'C');
    $pdf->Cell(75,5,utf8_decode($value['nombres']),1,0,'L');
    $pdf->Cell(35,5,utf8_decode($value['marca']),1,0,'L');
    $pdf->Cell(35,5,utf8_decode($value['modelo']),1,0,'L');
    $pdf->Cell(30,5,utf8_decode($value['ip_equipo']),1,1,'C');
    $i++;
}
$pdf->Output();
?>