$(document).ready(function () {
    $("#nueva_asignacion").click(function(e) {
        e.preventDefault();
        document.getElementById("form_asignacion").reset();
        $("#area_id").val(-1);
        $("#titulo").html('Registar Asignaciíon de Equipo');
        document.getElementById('guardar_asignacion').disabled=false;
        $('#modal_asignacion').modal('show');
    });
    $('#modal_asignacion').on('shown.bs.modal', function(e) {
        e.preventDefault();
        $("#id_personal").focus();
    });

    $("#guardar_asignacion").click(function () {
        if ($('#id_personal').val().trim() === '') {
            $('#id_personal').val("").focus();
            return false;
        }
        if ($('#id_equipo').val().trim() === '') {
            $('#id_equipo').val("").focus();
            return false;
        }
        if ($("#asignacion_id").val() == -1){
            var valor = 1;
        }else{
            valor = 2;
        }
        guardar(valor);
    });

    $("#reporte_pdf").click(function () {
        window.open('control/asignacion.php?accion=3', '_blank');
    });
});

function guardar(valor){
    document.getElementById('guardar_asignacion').disabled=true;
    $.ajax({
        url:'control/asignacion.php',
        data:'accion='+valor+'&'+$("#form_asignacion").serialize(),
        type:'post',
        success: function(data) {
            if (data==1) {
                if (valor == 1){
                    alertify.notify('<i class="fa fa-check"></i> Guardado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                } else {
                    alertify.notify('<i class="fa fa-check"></i> Actualizado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                }
                listado();
            } else {
                alertify.notify('<i class="fa fa-times"></i> Error al Guardar!!', 'error', 5, function(){  console.log('dismissed'); });
                listado();
            }
            $('#modal_asignacion').modal('hide');
        }
    });
    return false;
}

function editar_asignacion(id){
    document.getElementById("form_asignacion").reset();
    $("#titulo").html('Actualizar Asiganción de Equipo');
    $.ajax({
        url:'control/asignacion.php',
        data:'accion=4&id='+id,
        type:'post',
        success: function(info) {
            var datos = eval(info);
            $("#asignacion_id").val(datos[0]["id_equipo_persona"]);
            $("#id_personal").val(datos[0]["id_persona"]);
            $("#id_equipo").val(datos[0]["id_equipo"]);
            document.getElementById('guardar_asignacion').disabled=false;
            $('#modal_asignacion').modal('show');
        }
    });
}

var idmant = 0;
var tipo = "";
function confirmar_anular(id){
    idmant=id;
    tipo = "anular";
    $('#content_confirmacion').html('¿Esta sequro que desea Anular el Área?');
    $('#modal_confirmacion').modal('show');
}

function eliminar_area(){
    $.ajax({
        url:'control/area.php',
        data:'accion=5&id='+idmant+'&tipo='+tipo,
        type:'post',
        success: function(data) {
            if (data==1) {
                if (tipo == "anular"){
                    alertify.notify('<i class="fa fa-check"></i> Anulado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("A");
                }
                if (tipo == "restablecer"){
                    alertify.notify('<i class="fa fa-check"></i> Restablecido Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("A");
                }
                if (tipo == "eliminar"){
                    alertify.notify('<i class="fa fa-check"></i> Eliminado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("I");
                }
            }else{
                alertify.notify('<i class="fa fa-times"></i> Ha Ocurrido un Error!!', 'error', 5, function(){  console.log('dismissed'); });
                listado("A");
            }
            $("#modal_confirmacion").modal("hide");
        }
    });
}

function listado(){
    $.ajax({
        url:'control/asignacion.php',
        data:'accion=0',
        type:'post',
        success: function(data) {
            $("#content").empty().html(data);
            $("#dataTables-example").dataTable();
        }
    });
}