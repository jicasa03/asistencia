$(document).ready(function () {
    $("#nuevo_usuario").click(function(e) {
        e.preventDefault();
        document.getElementById("form_usuario").reset();
        $("#usuario_id").val(-1);
        $("#titulo").html('Registrar Usuario');
        document.getElementById('guardar_usuario').disabled=false;
        $('#modal_usuario').modal('show');
    });
    $('#modal_usuario').on('shown.bs.modal', function(e) {
        e.preventDefault();
        $("#nombres").focus();
    });

    $("#guardar_usuario").click(function () {
        if ($('#nombres').val().trim() === '') {
            $('#nombres').val("").focus();
            return false;
        }
        if ($('#usuario').val().trim() === '') {
            $('#usuario').val("").focus();
            return false;
        }
        if ($('#clave').val().trim() === '') {
            $('#clave').val("").focus();
            return false;
        }

        if ($("#usuario_id").val() == -1){
            var valor = 1;
        }else{
            valor = 2;
        }
        guardar(valor);
    });
});

function guardar(valor){
    document.getElementById('guardar_usuario').disabled=true;
    $.ajax({
        url:'control/usuario.php',
        data:'accion='+valor+'&'+$("#form_usuario").serialize(),
        type:'post',
        success: function(data) {
            if (data==1) {
                if (valor == 1){
                    alertify.notify('<i class="fa fa-check"></i> Guardado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                } else {
                    alertify.notify('<i class="fa fa-check"></i> Actualizado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                }
                listado("A");
                $('#modal_usuario').modal('hide');
            }
            if (data==2) {
                alertify.notify('<i class="fa fa-warning"></i> El Usuario : '+$("#usuario").val()+' !ya está Registrado', 'warning', 5, function(){  console.log('dismissed'); });
                $('#usuario').focus();
                document.getElementById('guardar_usuario').disabled=false;
            }
            if (data==0) {
                alertify.notify('<i class="fa fa-times"></i> Error al Guardar!!', 'error', 5, function(){  console.log('dismissed'); });
                listado("A");
                $('#modal_usuario').modal('hide');
            }

        }
    });
    return false;
}

function editar_usuario(id){
    document.getElementById("form_usuario").reset();
    $("#titulo").html('Actualizar Usuario');
    $.ajax({
        url:'control/usuario.php',
        data:'accion=4&id='+id,
        type:'post',
        success: function(info) {
            var datos = eval(info);
            $("#usuario_id").val(datos[0]["id_usuario"]);
            $("#nombres").val(datos[0]["nombres"]);
            $("#usuario").val(datos[0]["usuario"]);
            $("#clave").val(datos[0]["clave"]);
            document.getElementById('guardar_usuario').disabled=false;
            $('#modal_usuario').modal('show');
        }
    });
}

var idmant = 0;
var tipo = "";
function confirmar_anular(id){
    idmant=id;
    tipo = "anular";
    $('#content_confirmacion').html('¿Esta sequro que desea Anular el Usuario?');
    $('#modal_confirmacion').modal('show');
}

function confirmar_restablecer(id){
    idmant=id;
    tipo = "restablecer";
    $('#content_confirmacion').html('¿Esta sequro que desea Restablecer el Usuario?');
    $('#modal_confirmacion').modal('show');
}

function confirmar_eliminar(id){
    idmant=id;
    tipo = "eliminar";
    $('#content_confirmacion').html('¿Esta sequro que desea Elimar el Usuario?');
    $('#modal_confirmacion').modal('show');
}

function eliminar_area(){
    $.ajax({
        url:'control/usuario.php',
        data:'accion=5&id='+idmant+'&tipo='+tipo,
        type:'post',
        success: function(data) {
            if (data==1) {
                if (tipo == "anular"){
                    alertify.notify('<i class="fa fa-check"></i> Anulado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("A");
                }
                if (tipo == "restablecer"){
                    alertify.notify('<i class="fa fa-check"></i> Restablecido Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("A");
                }
                if (tipo == "eliminar"){
                    alertify.notify('<i class="fa fa-check"></i> Eliminado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("I");
                }
            }else{
                alertify.notify('<i class="fa fa-times"></i> Ha Ocurrido un Error!!', 'error', 5, function(){  console.log('dismissed'); });
                listado("A");
            }
            $("#modal_confirmacion").modal("hide");
        }
    });
}

function listado(estado){
    $.ajax({
        url:'control/usuario.php',
        data:'accion=0&estado='+estado,
        type:'post',
        success: function(data) {
            $("#content").empty().html(data);
            $("#dataTables-example").dataTable();
        }
    });
}