$(document).ready(function () {
    $("#nuevo_equipo_tipo_equipo").click(function(e) {
        e.preventDefault();
        document.getElementById("form_tipo_equipo").reset();
        $("#tipo_equipo_id").val(-1);
        $("#titulo").html('Registrar Tipo de Equipo');
        document.getElementById('guardar_tipo_equipo').disabled=false;
        $('#modal_tipo_equipo').modal('show');
    });
    $('#modal_tipo_equipo').on('shown.bs.modal', function(e) {
        e.preventDefault();
        $("#descripcion").focus();
    });

    $("#guardar_tipo_equipo").click(function () {
        if ($('#descripcion').val().trim() === '') {
            $('#descripcion').val("").focus();
            return false;
        }

        if ($("#tipo_equipo_id").val() == -1){
            var valor = 1;
        }else{
            valor = 2;
        }
        guardar(valor);
    });
});

function guardar(valor){
    document.getElementById('guardar_tipo_equipo').disabled=true;
    $.ajax({
        url:'control/tipo_equipo.php',
        data:'accion='+valor+'&'+$("#form_tipo_equipo").serialize(),
        type:'post',
        success: function(data) {
            if (data==1) {
                if (valor == 1){
                    alertify.notify('<i class="fa fa-check"></i> Guardado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                } else {
                    alertify.notify('<i class="fa fa-check"></i> Actualizado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                }
                listado("A");
                $('#modal_tipo_equipo').modal('hide');
            }
            if (data==2) {
                alertify.notify('<i class="fa fa-warning"></i> El Tipo de Equipo : '+$("#descripcion").val()+' !ya está Registrada', 'warning', 5, function(){  console.log('dismissed'); });
                $('#descripcion').focus();
                document.getElementById('guardar_tipo_equipo').disabled=false;
            }

            if (data==0) {
                alertify.notify('<i class="fa fa-times"></i> Error al Guardar!!', 'error', 5, function(){  console.log('dismissed'); });
                listado("A");
                $('#modal_tipo_equipo').modal('hide');
            }
        }
    }); return false;
}

function editar_tipo_equipo(id){
    document.getElementById("form_tipo_equipo").reset();
    $("#titulo").html('Actualizar Tipo de Equipo');
    $.ajax({
        url:'control/tipo_equipo.php',
        data:'accion=4&id='+id,
        type:'post',
        success: function(info) {
            var datos = eval(info);
            $("#tipo_equipo_id").val(datos[0]["id_tipo_equipo"]);
            $("#descripcion").val(datos[0]["descripcion"]);
            document.getElementById('guardar_tipo_equipo').disabled=false;
            $('#modal_tipo_equipo').modal('show');
        }
    });
}

var idmant = 0;
var tipo = "";
function confirmar_anular(id){
    idmant=id;
    tipo = "anular";
    $('#content_confirmacion').html('¿Esta sequro que desea Anular el Área?');
    $('#modal_confirmacion').modal('show');
}

function confirmar_restablecer(id){
    idmant=id;
    tipo = "restablecer";
    $('#content_confirmacion').html('¿Esta sequro que desea Restablecer el Área?');
    $('#modal_confirmacion').modal('show');
}

function confirmar_eliminar(id){
    idmant=id;
    tipo = "eliminar";
    $('#content_confirmacion').html('¿Esta sequro que desea Elimar el Área?');
    $('#modal_confirmacion').modal('show');
}

function eliminar_tipo_equipo(){
    $.ajax({
        url:'control/tipo_equipo.php',
        data:'accion=5&id='+idmant+'&tipo='+tipo,
        type:'post',
        success: function(data) {
            if (data==1) {
                if (tipo == "anular"){
                    alertify.notify('<i class="fa fa-check"></i> Anulado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("A");
                }
                if (tipo == "restablecer"){
                    alertify.notify('<i class="fa fa-check"></i> Restablecido Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("A");
                }
                if (tipo == "eliminar"){
                    alertify.notify('<i class="fa fa-check"></i> Eliminado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("I");
                }
            }else{
                alertify.notify('<i class="fa fa-times"></i> Ha Ocurrido un Error!!', 'error', 5, function(){  console.log('dismissed'); });
                listado("A");
            }
            $("#modal_confirmacion").modal("hide");
        }
    });
}

function listado(estado){
    $.ajax({
        url:'control/tipo_equipo.php',
        data:'accion=0&estado='+estado,
        type:'post',
        success: function(data) {
            $("#content").empty().html(data);
            $("#dataTables-example").dataTable();
        }
    });
}