$(document).ready(function () {
    $("#nuevo_equipo").click(function(e) {
        $(".botones").removeClass('show');
        $(".botones").addClass('hide');
        $("#titulo_pagina").html('Registrar Equipo');
        $.ajax({
            url:'control/equipo.php',
            data:'accion=7',
            type:'post',
            success: function(data) {
                $("#content").empty().html(data);
            }
        });
    });

    $("#cancelar").click(function() {
        location.reload(true);
    });

    $("#guardar_equipo").click(function () {
        if ($('#id_tipo_equipo').val().trim() === '') {
            $('#id_tipo_equipo').val("").focus();
            return false;
        }
        if ($('#marca').val().trim() === '') {
            $('#marca').val("").focus();
            return false;
        }
        if ($('#modelo').val().trim() === '') {
            $('#modelo').val("").focus();
            return false;
        }
        if ($('#id_estado').val().trim() === '') {
            $('#id_estado').val("").focus();
            return false;
        }

        if ($("#equipo_id").val() == -1){
            var valor = 1;
        }else{
            valor = 2;
        }
        guardar(valor);
    });
});

function guardar(valor){
    document.getElementById('guardar_equipo').disabled=true;
    $.ajax({
        url:'control/equipo.php',
        data:'accion='+valor+'&'+$("#form_equipo").serialize(),
        type:'post',
        success: function(data) {
            if (data==1) {
                if (valor == 1){
                    alertify.notify('<i class="fa fa-check"></i> Guardado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                } else {
                    alertify.notify('<i class="fa fa-check"></i> Actualizado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                }
                listado(1);
                $(".botones").removeClass('hide');
                $(".botones").addClass('show');
                $("#titulo_pagina").html('Lista de Equipos');
            }
            if (data==2){
                alertify.notify('<i class="fa fa-warning"></i> La : '+$("#ip_equipo").val()+' !ya esta asignada a un equipo', 'warning', 7, function(){  console.log('dismissed'); });
                $('#ip_equipo').focus();
                document.getElementById('guardar_equipo').disabled=false;
            }
            if (data==0){
                alertify.notify('<i class="fa fa-times"></i> Error al Guardar!!', 'error', 5, function(){  console.log('dismissed'); });
                listado(1);
                $(".botones").removeClass('hide');
                $(".botones").addClass('show');
                $("#titulo_pagina").html('Lista de Equipos');
            }
        }
    });
    return false;
}

function editar_equipo(id){
    $(".botones").removeClass('show');
    $(".botones").addClass('hide');
    $("#titulo_pagina").html('Registrar Equipo');
    $.ajax({
        url:'control/equipo.php',
        data:'accion=7&id='+id,
        type:'post',
        success: function(data) {
            $("#content").empty().html(data);
        }
    });
}

var idmant = 0;
var tipo = "";
function confirmar_anular(id){
    idmant=id;
    tipo = "anular";
    $('#content_confirmacion').html('¿Esta sequro que desea Anular el Área?');
    $('#modal_confirmacion').modal('show');
}

function confirmar_restablecer(id){
    idmant=id;
    tipo = "restablecer";
    $('#content_confirmacion').html('¿Esta sequro que desea Restablecer el Área?');
    $('#modal_confirmacion').modal('show');
}

function confirmar_eliminar(id){
    idmant=id;
    tipo = "eliminar";
    $('#content_confirmacion').html('¿Esta sequro que desea Elimar el Área?');
    $('#modal_confirmacion').modal('show');
}

function eliminar_tipo_equipo(){
    $.ajax({
        url:'control/tipo_equipo.php',
        data:'accion=5&id='+idmant+'&tipo='+tipo,
        type:'post',
        success: function(data) {
            if (data==1) {
                if (tipo == "anular"){
                    alertify.notify('<i class="fa fa-check"></i> Anulado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("A");
                }
                if (tipo == "restablecer"){
                    alertify.notify('<i class="fa fa-check"></i> Restablecido Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("A");
                }
                if (tipo == "eliminar"){
                    alertify.notify('<i class="fa fa-check"></i> Eliminado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("I");
                }
            }else{
                alertify.notify('<i class="fa fa-times"></i> Ha Ocurrido un Error!!', 'error', 5, function(){  console.log('dismissed'); });
                listado("A");
            }
            $("#modal_confirmacion").modal("hide");
        }
    });
}

function listado(estado){
    $.ajax({
        url:'control/equipo.php',
        data:'accion=0&estado='+estado,
        type:'post',
        success: function(data) {
            $("#content").empty().html(data);
            $("#dataTables-example").dataTable();
        }
    });
}