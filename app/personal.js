$(document).ready(function () {
    $("#nuevo_personal").click(function(e) {
        e.preventDefault();
        document.getElementById("form_personal").reset();
        $("#personal_id").val(-1);
        $("#radio5").attr('checked',true);
        $("#titulo").html('Registrar Personal');
        document.getElementById('guardar_personal').disabled=false;
        $('#modal_personal').modal('show');
    });
    $('#modal_personal').on('shown.bs.modal', function(e) {
        e.preventDefault();
        $("#nombres").focus();
    });

    $("#guardar_personal").click(function () {
        if ($('#nombres').val().trim() === '') {
            $('#nombres').val("").focus();
            return false;
        }

        if ($("#personal_id").val() == -1){
            var valor = 1;
        }else{
            valor = 2;
        }
        guardar(valor);
    });
});

function guardar(valor){
    document.getElementById('guardar_personal').disabled=true;
    $.ajax({
        url:'control/personal.php',
        data:'accion='+valor+'&'+$("#form_personal").serialize(),
        type:'post',
        success: function(data) {
            if (data==1) {
                if (valor == 1) {
                    alertify.notify('<i class="fa fa-check"></i> Guardado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                } else {
                    alertify.notify('<i class="fa fa-check"></i> Actualizado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                }
                listado("A");
                $('#modal_personal').modal('hide');
            }
            if (data==2) {
                alertify.notify('<i class="fa fa-warning"></i> La Persona : '+$("#nombres").val()+' !ya está Registrada', 'warning', 5, function(){  console.log('dismissed'); });
                $('#nombres').focus();
                document.getElementById('guardar_personal').disabled=false;
            }
            if (data==0) {
                alertify.notify('<i class="fa fa-times"></i> Error al Guardar!!', 'error', 5, function(){  console.log('dismissed'); });
                listado("A");
                $('#modal_personal').modal('hide');
            }
        }
    }); return false;
}

function editar_personal(id){
    document.getElementById("form_personal").reset();
    $("#titulo").html('Actualizar Personal');
    $.ajax({
        url:'control/personal.php',
        data:'accion=4&id='+id,
        type:'post',
        success: function(info) {
            var datos = eval(info);
            /*
            if (datos[0]["jefe"]=="SI"){
                $("#radio5").attr('checked',false);
                $("#radio6").attr('checked',true);
            } else {
                $("#radio6").attr('checked',false);
                $("#radio5").attr('checked',true);
            }*/
            document.getElementById('guardar_personal').disabled=false;
         
            $("#personal_id").val(datos[0]["id_persona"]);
            $("#nombres").val(datos[0]["nombres"]);
            $("#id_area").val(datos[0]["id_area"]);
            $("#id_tipo_persona").val(datos[0]["id_tipo_persona"]);
              $("#dni").val(datos[0]["dni"]);
            $('#modal_personal').modal('show');
        }
    });
}

var idmant = 0;
var tipo = "";
function confirmar_anular(id){
    idmant=id;
    tipo = "anular";
    $('#content_confirmacion').html('¿Esta sequro que desea Anular la Persona?');
    $('#modal_confirmacion').modal('show');
}

function confirmar_restablecer(id){
    idmant=id;
    tipo = "restablecer";
    $('#content_confirmacion').html('¿Esta sequro que desea Restablecer la Persona?');
    $('#modal_confirmacion').modal('show');
}

function confirmar_eliminar(id){
    idmant=id;
    tipo = "eliminar";
    $('#content_confirmacion').html('¿Esta sequro que desea Elimar la Persona?');
    $('#modal_confirmacion').modal('show');
}

function eliminar_personal(){
    $.ajax({
        url:'control/personal.php',
        data:'accion=5&id='+idmant+'&tipo='+tipo,
        type:'post',
        success: function(data) {
            if (data==1) {
                if (tipo == "anular"){
                    alertify.notify('<i class="fa fa-check"></i> Anulado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("A");
                }
                if (tipo == "restablecer"){
                    alertify.notify('<i class="fa fa-check"></i> Restablecido Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("A");
                }
                if (tipo == "eliminar"){
                    alertify.notify('<i class="fa fa-check"></i> Eliminado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("I");
                }
            }else{
                alertify.notify('<i class="fa fa-times"></i> Ha Ocurrido un Error!!', 'error', 5, function(){  console.log('dismissed'); });
                listado("A");
            }
            $("#modal_confirmacion").modal("hide");
        }
    });
}

function listado(estado){
    $.ajax({
        url:'control/personal.php',
        data:'accion=0&estado='+estado,
        type:'post',
        success: function(data) {
            $("#content").empty().html(data);
            $("#dataTables-example").dataTable();
        }
    });
}