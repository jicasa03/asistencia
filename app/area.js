$(document).ready(function () {
    $("#nueva_area").click(function(e) {
        e.preventDefault();
        document.getElementById("form_area").reset();
        $("#area_id").val(-1);
        $("#titulo").html('Registrar Area');
        document.getElementById('guardar_area').disabled=false;
        $('#modal_area').modal('show');
    });
    $('#modal_area').on('shown.bs.modal', function(e) {
        e.preventDefault();
        $("#descripcion").focus();
    });

    $("#guardar_area").click(function () {
        if ($('#descripcion').val().trim() === '') {
            $('#descripcion').val("").focus();
            return false;
        }

        if ($("#area_id").val() == -1){
            var valor = 1;
        }else{
            valor = 2;
        }
        guardar(valor);
    });
});

function guardar(valor){
    document.getElementById('guardar_area').disabled=true;
    $.ajax({
        url:'control/area.php',
        data:'accion='+valor+'&'+$("#form_area").serialize(),
        type:'post',
        success: function(data) {
            if (data==1) {
                if (valor == 1){
                    alertify.notify('<i class="fa fa-check"></i> Guardado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                } else {
                    alertify.notify('<i class="fa fa-check"></i> Actualizado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                }
                listado("A");
                $('#modal_area').modal('hide');
            }
            if (data==2) {
                alertify.notify('<i class="fa fa-warning"></i> El area : '+$("#descripcion").val()+' !ya está Registrada', 'warning', 5, function(){  console.log('dismissed'); });
                $('#descripcion').focus();
                document.getElementById('guardar_area').disabled=false;
            }
            if (data==0) {
                alertify.notify('<i class="fa fa-times"></i> Error al Guardar!!', 'error', 5, function(){  console.log('dismissed'); });
                listado("A");
                $('#modal_area').modal('hide');
            }

        }
    });
    return false;
}

function editar_area(id){
    document.getElementById("form_area").reset();
    $("#titulo").html('Actualizar Area');
    $.ajax({
        url:'control/area.php',
        data:'accion=4&id='+id,
        type:'post',
        success: function(info) {
            var datos = eval(info);
            $("#area_id").val(datos[0]["id_area"]);
            $("#descripcion").val(datos[0]["descripcion"]);
            document.getElementById('guardar_area').disabled=false;
            $('#modal_area').modal('show');
        }
    });
}

var idmant = 0;
var tipo = "";
function confirmar_anular(id){
    idmant=id;
    tipo = "anular";
    $('#content_confirmacion').html('¿Esta sequro que desea Anular el Área?');
    $('#modal_confirmacion').modal('show');
}

function confirmar_restablecer(id){
    idmant=id;
    tipo = "restablecer";
    $('#content_confirmacion').html('¿Esta sequro que desea Restablecer el Área?');
    $('#modal_confirmacion').modal('show');
}

function confirmar_eliminar(id){
    idmant=id;
    tipo = "eliminar";
    $('#content_confirmacion').html('¿Esta sequro que desea Elimar el Área?');
    $('#modal_confirmacion').modal('show');
}

function eliminar_area(){
    $.ajax({
        url:'control/area.php',
        data:'accion=5&id='+idmant+'&tipo='+tipo,
        type:'post',
        success: function(data) {
            if (data==1) {
                if (tipo == "anular"){
                    alertify.notify('<i class="fa fa-check"></i> Anulado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("A");
                }
                if (tipo == "restablecer"){
                    alertify.notify('<i class="fa fa-check"></i> Restablecido Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("A");
                }
                if (tipo == "eliminar"){
                    alertify.notify('<i class="fa fa-check"></i> Eliminado Correctamente!!', 'success', 5, function(){  console.log('dismissed'); });
                    listado("I");
                }
            }else{
                alertify.notify('<i class="fa fa-times"></i> Ha Ocurrido un Error!!', 'error', 5, function(){  console.log('dismissed'); });
                listado("A");
            }
            $("#modal_confirmacion").modal("hide");
        }
    });
}

function listado(estado){
    $.ajax({
        url:'control/area.php',
        data:'accion=0&estado='+estado,
        type:'post',
        success: function(data) {
            $("#content").empty().html(data);
            $("#dataTables-example").dataTable();
        }
    });
}