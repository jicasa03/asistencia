/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : gtbm_t

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 19/11/2019 14:37:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for area
-- ----------------------------
DROP TABLE IF EXISTS `area`;
CREATE TABLE `area`  (
  `id_area` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_area`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of area
-- ----------------------------
INSERT INTO `area` VALUES (1, 'GERENCIA', 'A');
INSERT INTO `area` VALUES (2, 'TRAMITE DOCUMENTARIO', 'A');
INSERT INTO `area` VALUES (3, 'SUB DIRECCION DE ADMINISTRACION', 'A');
INSERT INTO `area` VALUES (4, 'SOPORTE TECNICO INFORMATICO', 'A');
INSERT INTO `area` VALUES (5, 'CONTABILIDAD', 'A');
INSERT INTO `area` VALUES (6, 'RECURSOS HUMANOS', 'A');
INSERT INTO `area` VALUES (7, 'LOGISTICA', 'A');
INSERT INTO `area` VALUES (8, 'ALMACEN', 'A');
INSERT INTO `area` VALUES (9, 'ARCHIVO INSTITUCIONAL', 'A');
INSERT INTO `area` VALUES (10, 'CONTROL PATRIMONIAL', 'A');
INSERT INTO `area` VALUES (11, 'PRESUPUESTO Y ACONDICIONAMIENTO', 'A');
INSERT INTO `area` VALUES (12, 'ASESORIA LEGAL', 'A');
INSERT INTO `area` VALUES (13, 'DESARROLLO ECONOMICO', 'A');
INSERT INTO `area` VALUES (14, 'INFRAESTRUCTURA', 'A');
INSERT INTO `area` VALUES (15, 'TESORERIA', 'A');
INSERT INTO `area` VALUES (16, 'DIRECCION DE ARCHIVO SUB REGIONAL', 'A');
INSERT INTO `area` VALUES (17, 'DESARROLLO SOCIAL', 'A');
INSERT INTO `area` VALUES (18, 'SECRETARIA TECNICA', 'A');

-- ----------------------------
-- Table structure for asistencia
-- ----------------------------
DROP TABLE IF EXISTS `asistencia`;
CREATE TABLE `asistencia`  (
  `asistencia_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` int(255) NULL DEFAULT NULL,
  `turno_id` int(11) NULL DEFAULT NULL,
  `tipo_horario_id` int(11) NULL DEFAULT NULL,
  `fecha_hora` datetime(0) NULL DEFAULT NULL,
  `fecha` date NULL DEFAULT NULL,
  PRIMARY KEY (`asistencia_id`) USING BTREE,
  INDEX `turno_id`(`turno_id`) USING BTREE,
  INDEX `tipo_horario_id`(`tipo_horario_id`) USING BTREE,
  INDEX `id_persona`(`id_persona`) USING BTREE,
  CONSTRAINT `asistencia_ibfk_1` FOREIGN KEY (`turno_id`) REFERENCES `turno` (`turno_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `asistencia_ibfk_2` FOREIGN KEY (`tipo_horario_id`) REFERENCES `tipo_horario` (`tipo_horario_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `asistencia_ibfk_3` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of asistencia
-- ----------------------------
INSERT INTO `asistencia` VALUES (30, 3, 1, 1, '2019-10-07 03:05:16', '2019-10-07');
INSERT INTO `asistencia` VALUES (31, 3, 1, 2, '2019-10-07 03:05:19', '2019-10-07');
INSERT INTO `asistencia` VALUES (32, 3, 2, 1, '2019-10-07 03:05:23', '2019-10-07');
INSERT INTO `asistencia` VALUES (33, 3, 2, 2, '2019-10-07 03:05:26', '2019-10-07');
INSERT INTO `asistencia` VALUES (34, 1, 1, 1, '2019-11-19 20:33:16', '2019-11-19');
INSERT INTO `asistencia` VALUES (35, 1, 1, 2, '2019-11-19 20:33:24', '2019-11-19');
INSERT INTO `asistencia` VALUES (36, 1, 2, 1, '2019-11-19 20:33:26', '2019-11-19');
INSERT INTO `asistencia` VALUES (37, 1, 2, 2, '2019-11-19 20:33:30', '2019-11-19');

-- ----------------------------
-- Table structure for equipo
-- ----------------------------
DROP TABLE IF EXISTS `equipo`;
CREATE TABLE `equipo`  (
  `id_equipo` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_equipo` int(11) NULL DEFAULT NULL,
  `marca` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `modelo` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `usuario` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ip_equipo` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `caracteristicas` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_estado` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_equipo`) USING BTREE,
  INDEX `FK_TIPO_EQUIPO_EQUIPO`(`id_tipo_equipo`) USING BTREE,
  INDEX `FK_ESTADO_EQUIPO`(`id_estado`) USING BTREE,
  CONSTRAINT `FK_ESTADO_EQUIPO` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id_estado`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_TIPO_EQUIPO_EQUIPO` FOREIGN KEY (`id_tipo_equipo`) REFERENCES `tipo_equipo` (`id_tipo_equipo`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 67 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of equipo
-- ----------------------------
INSERT INTO `equipo` VALUES (1, 1, 'HP', 'PROLIANT ML110 G6', 'Server1\\Administrador', '192.168.88.5', 'MOTHERBOARD:PROLIANT ML110 G6\r\nCPU; INTEL CORE I3 530 2.93GHZ\r\nRAM:5GB \r\nHDD:DISCO0=149GB, DISCO1=465GB', 1);
INSERT INTO `equipo` VALUES (2, 1, 'HP', 'PROLIANT ML110 G7', 'GRSMS0018\\Administrador', '192.168.88.6', 'MOTHERBOARD:PROLIANT ML110 G7\r\nCPU: INTEL XEON E31220 3.10GHZ\r\nRAM:8GB \r\nHDD:DISCO0=465GB', 1);
INSERT INTO `equipo` VALUES (3, 1, 'DELL', 'POWEREDGE R610', 'Server2\\Administrador', '192.168.88.7', 'MOTHERBOARD:POWEREDGW R610\r\nCPU; INTEL XEON L5520 2.27GHZ\r\nRAM: 8GB \r\nHDD: DISCO0=272GB, DISCO1=930GB', 1);
INSERT INTO `equipo` VALUES (4, 2, 'ALTRON', 'ALTRON', 'DESKTOP-D8ACGS0\\SECRE-PATRIMONIO', '192.168.88.128', 'MOTHERBOARD:\r\nCPU; INTEL CORE 2 DUO E7300 2.66GHZ\r\nRAM: 2GB \r\nHDD: 1TB', 1);
INSERT INTO `equipo` VALUES (5, 2, 'ADVANCE', 'ADVANCE', 'PATRIMONIO\\PATRIMONIO', '192.168.88.129', 'MOTHERBOARD:H81M-H\r\nCPU; INTEL CORE I5 - 4460 3.20GHZ\r\nRAM: 4GB \r\nHDD: DISCO0=931GB', 1);
INSERT INTO `equipo` VALUES (6, 3, 'HP', 'LASERJET P1102W', '', '', 'SERIE: BRBSBBSG3N\r\nMODELO: CE657A', 1);
INSERT INTO `equipo` VALUES (7, 2, 'LG', 'SYSTEM PRODUCT NAME', 'JEFE-SDDE\\lpanduro', '192.168.88.169', 'MOTHERBOARD:SYSTEM PRODUCT NAME\r\nCPU: INTEL CORE I3-3220 3.30 GHZ\r\nRAM: 2GB \r\nHDD: DISCO0:465GB', 1);
INSERT INTO `equipo` VALUES (8, 3, 'HP', 'LASERT PRO400 M40LN', '', '192.168.88.127', 'SERIE: BRFSF2BNXY\r\nMODELO: CZ195A', 1);
INSERT INTO `equipo` VALUES (9, 2, 'LG', 'LG', 'DESARROLLOSOCIAL\\desarrollo', '192.168.88.226', 'MOTHERBOARD:\r\nCPU; INTEL CORE I5-3470 3.20GHZ\r\nRAM:4GB \r\nHDD: DISCO0: 1TB', 1);
INSERT INTO `equipo` VALUES (10, 6, 'TOSHIBA', 'SATELITE', 'Planificacion/ograndez', '192.168.88.170', 'MOTHERBOARD:SATELITE A505\r\nCPU; INTEL CORE I3 M330 2.13GHZ\r\nRAM: 4GB \r\nHDD:DISCO0=465GB', 1);
INSERT INTO `equipo` VALUES (11, 3, 'HP', 'LASERJET P1102W', '', 'SIN IP', 'SERIE: BRBSP68674\r\nMODELO: CE657A', 1);
INSERT INTO `equipo` VALUES (12, 4, 'CANON', 'IMAGE RUNNER 2202N', '', '00', '', 2);
INSERT INTO `equipo` VALUES (13, 6, 'HP', 'PAVILION DV5 NOTEBOOK PC', 'Reten-HP\\administrador', '192.168.88.227', 'MOTHERBOARD: HP PAVILION DV5 NOTEBOOK PC\r\nCPU; INTEL CORE I5 M450 2.40GHZ\r\nRAM:4GB \r\nHDD:DISCO0=465GB', 1);
INSERT INTO `equipo` VALUES (14, 6, 'LENOVO', 'B40', 'DESKTOP-B33IU4R', '192.168.88.225', 'MOTHERBOARD:80F6\r\nCPU; INTEL CORE I5-5200U 2.20GHZ\r\nRAM:4GB \r\nHDD:DISCO0=465GB, DISCO1=14GB', 1);
INSERT INTO `equipo` VALUES (15, 6, 'TOSHIBA', 'SATELITE L635', 'DESKTOP-A2JDPEF\\ALMACEN', '192.168.88.99', 'MOTHERBOARD:SATELITE L635\r\nCPU; INTEL CORE I3 M530 2.27GHZ\r\nRAM:3GB \r\nHDD:DISCO0=298GB, DISCO1=931GB', 1);
INSERT INTO `equipo` VALUES (16, 2, 'LG', 'CYBERTEL', 'Jefe-Almacen\\volivares', '192.168.88.100', 'MOTHERBOARD: DB85FL\r\nCPU; INTEL CORE I7-4770 3.40GHZ\r\nRAM:4GB \r\nHDD:DISCO0=465GB', 1);
INSERT INTO `equipo` VALUES (17, 3, 'HP', 'LASER JET P2015DN', '', '-', 'SERIE: BRBS79DG54\r\nMODELO: CB368A', 1);
INSERT INTO `equipo` VALUES (18, 3, 'HP', 'LASERJET PRO MFP M127FN', '', '--', 'SERIE: BRBS6VVHC\r\nMODELO: CZ181A', 1);
INSERT INTO `equipo` VALUES (19, 6, 'LENOVO', 'V310', 'PRESUPUESTO', '192.168.88.57', 'MOTHERBOARD: 80SY\r\nCPU; INTEL CORE i7-6500U 2.6GHz\r\nRAM:  8GB \r\nHDD:  1TB', 1);
INSERT INTO `equipo` VALUES (20, 6, 'TOSHIBA', 'SATELLITE', 'Almacen\\ssantamaria', '192.168.88.101', 'MOTHERBOARD: SATELLITE U505\r\nCPU; INTEL CORE I3 M330 2.13GHZ\r\nRAM:4GB \r\nHDD:DISCO0=298GB', 1);
INSERT INTO `equipo` VALUES (21, 6, 'LENOVO', 'B40', 'LAPTOPGERENCIA\\Administrador.000', '192.168.88.130', 'MOTHERBOARD: 80F6\r\nCPU; INTEL CORE I5-5200U 2.20GHZ\r\nRAM:4GB \r\nHDD:DISCO0=465GB', 1);
INSERT INTO `equipo` VALUES (22, 2, 'LG', 'DALTONE', 'Asis-Adm\\administrador', '192.168.88.45', 'MOTHERBOARD:DG41RQ__\r\nCPU: INTEL CORE 2DUO E7500 2.93GHZ\r\nRAM: 2GB \r\nHDD: DISCO0= 465GB', 1);
INSERT INTO `equipo` VALUES (23, 2, 'LG', 'SYSTEM PRODUCT NAME', 'ADMIN-SEC\\dnavarro', '192.168.88.44', 'MOTHERBOARD:SYSTEM PRODUCT NAME\r\nCPU; INTEL PENTIUM G2020 2.90GHZ\r\nRAM:4GB \r\nHDD:DISCO0=465GB', 1);
INSERT INTO `equipo` VALUES (24, 6, 'HP', 'PROBOOK', 'DESKTOP-2CULFC0\\ADMINISTRACION', '192.168.88.43', 'MOTHERBOARD:HP PROBOOK 450G5\r\nCPU; INTEL CORE I7-8550 1.80GHZ\r\nRAM:8GB \r\nHDD:DISCO0=931GB', 1);
INSERT INTO `equipo` VALUES (25, 5, 'EPSON', 'L575', '', '192.168.88.126', '', 1);
INSERT INTO `equipo` VALUES (26, 6, 'Lenovo', 'Z50', 'ASIST INFRA', '192.168.88.', 'LENOVO 20354\r\nProcesador: Intel Core i7-4510U 2.10 Ghz\r\nRam: 16 GB\r\nHDD: 1TB', 1);
INSERT INTO `equipo` VALUES (27, 6, 'TOSHIBA', 'Satellite A505', 'USER03', '192.168.88.85', 'TOSHIBA SATELLITE A505\r\nCPU: INTEL CORE i3 2.13Ghz\r\nRAM: 4GB\r\nHDD: 500GB\r\n', 1);
INSERT INTO `equipo` VALUES (28, 7, 'KYOCERA', 'ECOSYS M3040idn', '', '---', 'KYOCERA ECOSYS M3040idn\r\n', 1);
INSERT INTO `equipo` VALUES (29, 2, 'Gigabyte', 'H81M-H', 'PRESUPUESTO', '192.168.88.87', 'MOTHERBOARD: H81M-H\r\nCPU: INTEL CORE i7-4790 3.6 Ghz\r\nRAM: 8GB\r\nHDD: 1TB', 1);
INSERT INTO `equipo` VALUES (30, 2, 'HP', 'ELITEDESK 800G1 SFF', 'ljimenez', '192.168.88.88', 'MOTHERBOAR:  ELITEDESK 800G1\r\nCPU:  INTEL CORE i7-4790 3.6 GHz\r\nRAM:  8GB\r\nHDD:  1TB\r\n', 1);
INSERT INTO `equipo` VALUES (31, 2, 'DESCONOCIDO', 'DESCONOCIDO', 'ASIST-LOGISTICA', '192.168.88.86', 'MOTHERBOARD:  \r\nCPU: INTEL CORE 2 DUO 2.5 GHz\r\nRAM:  4GB\r\nHDD:  150GB  ', 1);
INSERT INTO `equipo` VALUES (32, 2, 'GIBABYTE', 'H81M-H', 'mramirez.SRBM-T', '192.168.88.141', 'MOTHERBOARD:  H81M-H\r\nCPU:  INTEL CORE i7-4790 3.6 GHz\r\nRAM:  8GB\r\nHDD:  1TB', 1);
INSERT INTO `equipo` VALUES (33, 2, 'HP', 'PRODESK 600G1 SFF', 'mpinedo', '192.168.88.197', 'MOTHERBOARD:  PRODESK 600G1 SFF\r\nCPU:  INTEL CORE i5-4570 3.2 GHz\r\nRAM:  4GB\r\nHDD:  500GB', 1);
INSERT INTO `equipo` VALUES (34, 6, 'LENOVO', 'B50-70', 'administrador', '192.168.88.59', 'MOTHERBOARD:  LENOVO 20384\r\nCPU:  INTEL CORE i5-4200  2.3 GHz\r\nRAM:  4GB\r\nHDD:  500GB', 1);
INSERT INTO `equipo` VALUES (35, 2, 'INTEL', 'DH61CR', 'Administrador', '192.168.88.61', 'MOTHERBOARD:  DH61CR\r\nCPU:  INTEL CORE i5-3470 3.2GHz\r\nRAM:  4GB\r\nHDD:  1TB', 1);
INSERT INTO `equipo` VALUES (36, 2, 'DESCONOCIDO', 'DESCONOCIDO', 'ASIS CONTABILIDAD', '192.168.88.58', 'MOTHERBOARD:  \r\nCPU:  INTEL PENTIUM CPU G2020 2.9GHz\r\nRAM:  4GB\r\nHDD:  500GB', 1);
INSERT INTO `equipo` VALUES (37, 2, 'INTEL', 'DH61CR', 'mculqui', '192.168.88.71', 'MOTHERBOARD:  DH61CR\r\nCPU:  INTEL CORE i5-3470 3.2GHz\r\nRAM:  4GB\r\nHDD:  500GB', 1);
INSERT INTO `equipo` VALUES (38, 2, 'GIGABYTE', 'H81M-H', 'nsaavedra', '192.168.88.74', 'MOTHERBOARD:  H81M-H\r\nCPU:  INTEL CORE i5-4460 3.2GHz\r\nRAM:  8GB\r\nHDD:  1TB', 1);
INSERT INTO `equipo` VALUES (39, 2, 'GIGABYTE', 'H81M-H', 'Jefe de personal', '192.168.75', 'MOTHERBOARD:  H81M-H\r\nCPU:  INTEL CORE i5-4470  3.1GHz\r\nRAM:  8GB\r\nHDD:  1TB', 1);
INSERT INTO `equipo` VALUES (40, 5, 'EPSON', 'L355', '', '----', 'EPSON L355 MULTIFUNCIONAL', 1);
INSERT INTO `equipo` VALUES (41, 5, 'EPSON', 'L575', '', '-----', 'EPSON ECOTANK L575  MULTIFUNCIONAL', 1);
INSERT INTO `equipo` VALUES (42, 6, 'LENOVO', '80F6', 'Administrador', '192.168.88.17', 'MOTHERBOARD:  LENOVO 80F6\r\nCPU:  INTEL CORE i5-5200U 2.2GHz\r\nRAM:  4GB\r\nHDD:  500GB', 1);
INSERT INTO `equipo` VALUES (43, 6, 'TOSHIBA', 'SATELLITE C55-C', 'usuario', '192.168.88.15', 'MOTHERBOARD:  TOSHIBA SATELLITE C55-C\r\nCPU:  INTEL CORE i5-5200U 2.2GHz\r\nRAM:  4GB\r\nHDD:  500GB', 1);
INSERT INTO `equipo` VALUES (44, 2, 'DESCONOCIDO', 'DESCONOCIDO', 'Neliasi', '192.168.88.23', 'MOTHERBOARD:  \r\nCPU:  INTEL CORE i5 3.2GHz\r\nRAM:  4GB\r\nHDD:  500GB', 1);
INSERT INTO `equipo` VALUES (45, 5, 'EPSON', 'L575', '', '------', 'EPSON L575 MULTIFUNCIONAL', 1);
INSERT INTO `equipo` VALUES (46, 6, 'TOSHIBA SATELLITE', 'S55T-A5132', 'INFRA2', '192.168.88.185', 'MOTHERBOARD:  TOSHIBA SATELLITE S55T-A5132\r\nCPU:  INTEL CORE i7-4700MQ  2.4GHz\r\nRAM:  12GB\r\nHDD:  750GB', 1);
INSERT INTO `equipo` VALUES (47, 8, 'CANON', 'imageFORMULA DR-M160II', '', '--------', 'CANON imageFORMULA DR-M160II\r\nSCANER', 1);
INSERT INTO `equipo` VALUES (48, 6, 'HP', 'HP PROBOOK 450G5', 'INFRA_2', '192.168.88.186', 'MOTHER BOARD:  HP PROBOOK 450G5\r\nCPU:  INTEL CORE i7-8550U  2.0 GHz\r\nRAM:  8GB\r\nHDD:  1TB', 1);
INSERT INTO `equipo` VALUES (49, 6, 'HP', 'HP ENVY DV6', 'CFLORES', '192.168.88.188', 'MOTHERBOARD: HP ENVY DV6\r\nCPU:  INTEL CORE i7-3630QM  2.4GHZ\r\nRAM:  12GB\r\nHDD: 1TB', 1);
INSERT INTO `equipo` VALUES (50, 5, 'EPSON', 'L575', '', '192.168.88.122', 'EPSON L575 MULTIFUNCIONAL', 1);
INSERT INTO `equipo` VALUES (51, 6, 'HP', 'HP ENVY DV6', 'TREQUE', '192.168.88.189', 'MOTHERBOARD: HP ENVY DV6\r\nCPU:  INTEL CORE i7-3630QM  2.4GHz\r\nRAM:  12GB\r\nHDD:  1TB', 1);
INSERT INTO `equipo` VALUES (52, 3, 'HP', 'LASER JET PRO MFP M127FN', '', '192.168.001.125', 'HP LASER JET PRO MFP M127FN', 1);
INSERT INTO `equipo` VALUES (53, 2, 'DESCONOCIDO', 'DESCONOCIDO', 'ASESOR LEGAL', '192.168.88.184', 'MOTHERBOARD:  \r\nCPU:  INTEL CORE i5-5200 3.3GHz\r\nRAM: 2GB\r\nHDD: 1TB', 1);
INSERT INTO `equipo` VALUES (54, 2, 'CYBERTEL', 'P17G', 'MJRAMIREZ', '192.168.88.192', 'MOTHERBOARD:  P17G\r\nCPU:  INTEL CELERON 1.8GHz\r\nRAM: 2GB\r\nHDD:  500GB', 1);
INSERT INTO `equipo` VALUES (55, 2, 'DESCONOCIDO', 'DESCONOCIDO', 'CSAAVEDRA', '192.168.88.193', 'MOTHERBOARD:\r\nCPU:  INTEL CORE i5-3740 3.2GHz\r\nRAM:  2GB\r\nHDD: 1TB', 1);
INSERT INTO `equipo` VALUES (56, 3, 'HP', 'LASER JET PRO MFP M127FN', '', '192.168.88.131', 'HP LASER JET PRO MFP M127FN\r\nMULTIFUNCIONAL', 1);
INSERT INTO `equipo` VALUES (57, 6, 'LENOVO', 'Z50 - 20354', 'ASIST INFRA', '-----------', 'MOTHERBOARD:  LENOVO Z50 20354\r\nCPU:  INTEL CORE i7-4510U 2.6 GHz\r\nRAM:  16GB\r\nHDD:  1TB', 1);
INSERT INTO `equipo` VALUES (58, 6, 'HP', 'COMPAQ 610', 'INFRA03', '192.168.88.194', 'MOTHERBOARD:  HP COMPAQ610\r\nCPU:  INTEL CORE 2 DUO 2.0 GHz\r\nRAM:  3GB\r\nHDD:  250GB', 1);
INSERT INTO `equipo` VALUES (59, 6, 'LENOVO', '80SR', 'CAYO', '192.168.88.195', 'MOTHERBOARD:  LENOVO 80SR\r\nCPU:  INTEL CORE i7-6500U 2.6 GHz\r\nRAM:  4GB\r\nHDD: 1TB', 1);
INSERT INTO `equipo` VALUES (60, 6, 'HP', 'PROBOOK 450G5', 'LAPTOP-HP', '192.168.88.196', 'MOTHERBOARD:  HP PROBOOK 450G5\r\nCPU:  INTEL CORE i7-8550U 2.0GHz\r\nRAM:  8GB\r\nHDD: 1TB', 1);
INSERT INTO `equipo` VALUES (61, 11, 'SHARP', 'MX-M465', '', '------------', 'FOTOCOPIADORA SHARP MX-M465', 1);
INSERT INTO `equipo` VALUES (62, 5, 'HP', 'LASERJET P1006', '', '---------', 'HP LASERJET P1006', 1);
INSERT INTO `equipo` VALUES (63, 6, 'LENOVO', '20384', 'USUARIO', '192.168.88.187', 'MOTHERBOARD: LENOVO 20384\r\nCPU:  INTEL CORE i5-4200 2.3GHz\r\nRAM:  4GB\r\nHDD:  500GB', 1);
INSERT INTO `equipo` VALUES (64, 6, 'DELL', 'INSPIRON 5567', 'INFRAESTRUCTURA', '192.168.88.183', 'MOTHERBOARD: DELL INSPIRON 5567\r\nCPU:  INTEL CORE i7-7500U  2.9GHz\r\nRAM: 16GB\r\nHDD: 2TB', 1);
INSERT INTO `equipo` VALUES (65, 2, 'ADVANCE', 'H81M-H', 'SECRE INFRA', '192.168.88.70', 'MOTHERBOARD:  H81M-H\r\nCPU:  INTEL CORE i5-4460 3.2GHz\r\nRAM:  8GB\r\nHDD:  1TB', 1);
INSERT INTO `equipo` VALUES (66, 3, 'HP', 'LASERJET PRO MFP M127FN', '', '192.168.88.190', 'HP LASERJET PRO MFP M127FN\r\nMULTIFUNCIONAL', 1);

-- ----------------------------
-- Table structure for equipo_persona
-- ----------------------------
DROP TABLE IF EXISTS `equipo_persona`;
CREATE TABLE `equipo_persona`  (
  `id_equipo_persona` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` int(11) NULL DEFAULT NULL,
  `id_equipo` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_equipo_persona`) USING BTREE,
  INDEX `FK_EQUIPO_PERSONA_PERSONA`(`id_persona`) USING BTREE,
  INDEX `FK_EQUIPO_PERSONA_EQUIPO`(`id_equipo`) USING BTREE,
  CONSTRAINT `FK_EQUIPO_PERSONA_EQUIPO` FOREIGN KEY (`id_equipo`) REFERENCES `equipo` (`id_equipo`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_EQUIPO_PERSONA_PERSONA` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`id_persona`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 65 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of equipo_persona
-- ----------------------------
INSERT INTO `equipo_persona` VALUES (1, 4, 1);
INSERT INTO `equipo_persona` VALUES (2, 4, 2);
INSERT INTO `equipo_persona` VALUES (3, 4, 3);
INSERT INTO `equipo_persona` VALUES (4, 30, 4);
INSERT INTO `equipo_persona` VALUES (5, 9, 5);
INSERT INTO `equipo_persona` VALUES (6, 9, 6);
INSERT INTO `equipo_persona` VALUES (7, 37, 8);
INSERT INTO `equipo_persona` VALUES (8, 24, 7);
INSERT INTO `equipo_persona` VALUES (9, 29, 9);
INSERT INTO `equipo_persona` VALUES (10, 12, 10);
INSERT INTO `equipo_persona` VALUES (11, 12, 11);
INSERT INTO `equipo_persona` VALUES (12, 37, 13);
INSERT INTO `equipo_persona` VALUES (13, 37, 14);
INSERT INTO `equipo_persona` VALUES (14, 9, 12);
INSERT INTO `equipo_persona` VALUES (15, 7, 15);
INSERT INTO `equipo_persona` VALUES (16, 7, 17);
INSERT INTO `equipo_persona` VALUES (17, 7, 18);
INSERT INTO `equipo_persona` VALUES (18, 31, 16);
INSERT INTO `equipo_persona` VALUES (19, 5, 19);
INSERT INTO `equipo_persona` VALUES (20, 7, 20);
INSERT INTO `equipo_persona` VALUES (21, 9, 21);
INSERT INTO `equipo_persona` VALUES (22, 27, 23);
INSERT INTO `equipo_persona` VALUES (23, 3, 22);
INSERT INTO `equipo_persona` VALUES (24, 3, 24);
INSERT INTO `equipo_persona` VALUES (25, 3, 25);
INSERT INTO `equipo_persona` VALUES (26, 13, 26);
INSERT INTO `equipo_persona` VALUES (27, 40, 27);
INSERT INTO `equipo_persona` VALUES (28, 41, 29);
INSERT INTO `equipo_persona` VALUES (29, 6, 30);
INSERT INTO `equipo_persona` VALUES (30, 42, 31);
INSERT INTO `equipo_persona` VALUES (31, 10, 32);
INSERT INTO `equipo_persona` VALUES (32, 15, 33);
INSERT INTO `equipo_persona` VALUES (33, 43, 19);
INSERT INTO `equipo_persona` VALUES (34, 44, 34);
INSERT INTO `equipo_persona` VALUES (35, 5, 35);
INSERT INTO `equipo_persona` VALUES (36, 45, 36);
INSERT INTO `equipo_persona` VALUES (37, 18, 37);
INSERT INTO `equipo_persona` VALUES (38, 32, 38);
INSERT INTO `equipo_persona` VALUES (39, 16, 39);
INSERT INTO `equipo_persona` VALUES (40, 16, 40);
INSERT INTO `equipo_persona` VALUES (41, 32, 41);
INSERT INTO `equipo_persona` VALUES (42, 11, 42);
INSERT INTO `equipo_persona` VALUES (43, 34, 43);
INSERT INTO `equipo_persona` VALUES (44, 34, 44);
INSERT INTO `equipo_persona` VALUES (45, 34, 45);
INSERT INTO `equipo_persona` VALUES (46, 46, 46);
INSERT INTO `equipo_persona` VALUES (47, 46, 47);
INSERT INTO `equipo_persona` VALUES (48, 47, 48);
INSERT INTO `equipo_persona` VALUES (49, 48, 49);
INSERT INTO `equipo_persona` VALUES (50, 48, 50);
INSERT INTO `equipo_persona` VALUES (51, 25, 51);
INSERT INTO `equipo_persona` VALUES (52, 25, 52);
INSERT INTO `equipo_persona` VALUES (53, 35, 53);
INSERT INTO `equipo_persona` VALUES (54, 35, 54);
INSERT INTO `equipo_persona` VALUES (55, 35, 55);
INSERT INTO `equipo_persona` VALUES (56, 35, 56);
INSERT INTO `equipo_persona` VALUES (57, 49, 57);
INSERT INTO `equipo_persona` VALUES (58, 50, 58);
INSERT INTO `equipo_persona` VALUES (59, 51, 59);
INSERT INTO `equipo_persona` VALUES (60, 50, 61);
INSERT INTO `equipo_persona` VALUES (61, 52, 63);
INSERT INTO `equipo_persona` VALUES (62, 52, 64);
INSERT INTO `equipo_persona` VALUES (63, 21, 65);
INSERT INTO `equipo_persona` VALUES (64, 21, 66);

-- ----------------------------
-- Table structure for estado
-- ----------------------------
DROP TABLE IF EXISTS `estado`;
CREATE TABLE `estado`  (
  `id_estado` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_estado`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of estado
-- ----------------------------
INSERT INTO `estado` VALUES (1, 'OPERATIVO', 'A');
INSERT INTO `estado` VALUES (2, 'INOPERATIVO', 'A');
INSERT INTO `estado` VALUES (3, 'DESFASADO', 'A');
INSERT INTO `estado` VALUES (4, 'DE BAJA', 'A');

-- ----------------------------
-- Table structure for persona
-- ----------------------------
DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona`  (
  `id_persona` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_tipo_persona` int(11) NULL DEFAULT NULL,
  `id_area` int(11) NULL DEFAULT NULL,
  `estado` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dni` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_persona`) USING BTREE,
  INDEX `FK_TIPO_PERSONA_PERSONA`(`id_tipo_persona`) USING BTREE,
  INDEX `FK_AREA_PERSONA`(`id_area`) USING BTREE,
  CONSTRAINT `FK_AREA_PERSONA` FOREIGN KEY (`id_area`) REFERENCES `area` (`id_area`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_TIPO_PERSONA_PERSONA` FOREIGN KEY (`id_tipo_persona`) REFERENCES `tipo_persona` (`id_tipo_persona`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of persona
-- ----------------------------
INSERT INTO `persona` VALUES (1, 'JHON SANDER ALEGRÃA ANGULO', 1, 1, 'A', '75270586');
INSERT INTO `persona` VALUES (2, 'JUAN RODRIGUEZ DÃVILA', 2, 16, 'A', NULL);
INSERT INTO `persona` VALUES (3, 'ABRAHAM MIRANDA SAAVEDRA', 1, 3, 'A', '47636281');
INSERT INTO `persona` VALUES (4, 'JORGE EMILIO MANRIQUE MACEDO', 1, 4, 'A', NULL);
INSERT INTO `persona` VALUES (5, 'FLOR MARGARITA BARCO GUERRERO', 1, 5, 'A', NULL);
INSERT INTO `persona` VALUES (6, 'LEIDI JIMÃ‰NEZ RIVAS', 1, 7, 'A', NULL);
INSERT INTO `persona` VALUES (7, 'GENIX SEGUNDO GARCIA BARTRA', 1, 8, 'A', NULL);
INSERT INTO `persona` VALUES (8, 'BELEN TAPULLIMA YSHUIZA', 1, 9, 'A', NULL);
INSERT INTO `persona` VALUES (9, 'BENITO URRUTIA REINEL', 1, 10, 'A', NULL);
INSERT INTO `persona` VALUES (10, 'JUAN CARLOS CAVERO ROJAS ', 1, 11, 'A', NULL);
INSERT INTO `persona` VALUES (11, 'JENIFER FLORES TELLO', 1, 12, 'A', NULL);
INSERT INTO `persona` VALUES (12, 'OLGAR GRANDEZ ARMAS', 1, 13, 'A', NULL);
INSERT INTO `persona` VALUES (13, 'RONALD ANTONIO PAREDES AREVALO', 1, 14, 'A', NULL);
INSERT INTO `persona` VALUES (14, 'MARLENI DEL ÃGUILA RUIZ DE SÃNCHEZ', 1, 16, 'A', NULL);
INSERT INTO `persona` VALUES (15, 'ANTONIO CARDENAS SOTO', 2, 15, 'A', NULL);
INSERT INTO `persona` VALUES (16, 'LEYDI CARITO ORDOÃ‘EZ RONCAL', 1, 6, 'A', NULL);
INSERT INTO `persona` VALUES (17, 'ROSA ANGELICA CABALLERO MENDOZA DE ANAYA', 1, 15, 'A', NULL);
INSERT INTO `persona` VALUES (18, 'MARIA ANTONIA CULQUI DE GRANDEZ', 2, 6, 'A', NULL);
INSERT INTO `persona` VALUES (19, 'JOSE EDGAR GARCIA RIOS', 2, 1, 'A', NULL);
INSERT INTO `persona` VALUES (20, 'CESAR AUGUSTO CAVERO NAPUCHI', 2, 16, 'A', NULL);
INSERT INTO `persona` VALUES (21, 'DOLORES ALFONSINA NAVARRO PEREZ', 2, 14, 'A', NULL);
INSERT INTO `persona` VALUES (22, 'TERESA NAVARRO LOPEZ', 1, 2, 'A', NULL);
INSERT INTO `persona` VALUES (23, 'VICTOR LUIS ALBERTO OLIVARES VIDAL', 2, 18, 'A', NULL);
INSERT INTO `persona` VALUES (24, 'LUIS BENIGNO PANDURO DIAZ', 2, 13, 'A', NULL);
INSERT INTO `persona` VALUES (25, 'TIMOTEO REQUE Ã‘IQUEN', 2, 14, 'A', NULL);
INSERT INTO `persona` VALUES (26, 'MARIA LUISA RUBIO CUELLAR', 2, 9, 'A', NULL);
INSERT INTO `persona` VALUES (27, 'SOCORRO RUIZ PAREDES ', 2, 3, 'A', NULL);
INSERT INTO `persona` VALUES (28, 'SAMUEL RUIZ RUIZ', 2, 1, 'A', NULL);
INSERT INTO `persona` VALUES (29, 'NANCY SAAVEDRA SANCHEZ', 2, 17, 'A', NULL);
INSERT INTO `persona` VALUES (30, 'MARIA HERMELINDA SANCHEZ AREVALO', 2, 10, 'A', NULL);
INSERT INTO `persona` VALUES (31, 'AUREA NATIVIDAD TUESTA DE CHAVEZ', 2, 8, 'A', NULL);
INSERT INTO `persona` VALUES (32, 'FERNANDO SAAVEDRA PEÃ‘A', 2, 6, 'A', NULL);
INSERT INTO `persona` VALUES (33, 'RICHARD VASQUEZ TORRES', 2, 7, 'A', NULL);
INSERT INTO `persona` VALUES (34, 'LING CHRIS RODRIGUEZ PEZO', 2, 1, 'A', NULL);
INSERT INTO `persona` VALUES (35, 'MARIA JULIA RAMIREZ HERRERA', 1, 18, 'A', NULL);
INSERT INTO `persona` VALUES (36, 'JOSE ANTONIO ALARCON LEON', 2, 2, 'A', NULL);
INSERT INTO `persona` VALUES (37, 'CARMELA SAAVEDRA PAREDES', 1, 17, 'A', NULL);
INSERT INTO `persona` VALUES (38, 'GENRRY TUESTA REATEGUI', 2, 3, 'A', NULL);
INSERT INTO `persona` VALUES (39, 'JAVIER TUESTA REATEGUI', 2, 3, 'A', NULL);
INSERT INTO `persona` VALUES (40, 'DANIELA SILVA BAUTISTA', 2, 7, 'A', NULL);
INSERT INTO `persona` VALUES (41, 'JAVIER LOPEZ SANCHEZ', 2, 7, 'A', NULL);
INSERT INTO `persona` VALUES (42, 'KEVIN ACUÃ‘A LUNA', 2, 7, 'A', NULL);
INSERT INTO `persona` VALUES (43, 'MARGARITA GARCIA CHISQUIPAMA', 2, 5, 'A', NULL);
INSERT INTO `persona` VALUES (44, 'FRANCIS GRANDEZ', 2, 5, 'A', NULL);
INSERT INTO `persona` VALUES (45, 'FIORELA SANGAMA PISCO', 2, 5, 'A', NULL);
INSERT INTO `persona` VALUES (46, 'PERCY GARCIA MONTES', 2, 14, 'A', NULL);
INSERT INTO `persona` VALUES (47, 'DONY HANS FLORES GARCIA', 2, 14, 'A', NULL);
INSERT INTO `persona` VALUES (48, 'KARLA ESTEFANY TANG PEZO', 2, 14, 'A', NULL);
INSERT INTO `persona` VALUES (49, 'MICHAEL SANCHEZ PUTPAÃ‘A', 2, 14, 'A', NULL);
INSERT INTO `persona` VALUES (50, 'LUIS ALFREDO GUZMAN RIOS', 2, 14, 'A', NULL);
INSERT INTO `persona` VALUES (51, 'JESSICA DEL PILAR SILVA SANCHEZ', 2, 14, 'A', NULL);
INSERT INTO `persona` VALUES (52, 'ELMER RAFAEL SANCHEZ CHAVEZ', 1, 14, 'A', NULL);
INSERT INTO `persona` VALUES (53, 'eqweqwe', 1, 1, 'A', '31231231');
INSERT INTO `persona` VALUES (54, NULL, NULL, NULL, 'A', NULL);

-- ----------------------------
-- Table structure for tipo_equipo
-- ----------------------------
DROP TABLE IF EXISTS `tipo_equipo`;
CREATE TABLE `tipo_equipo`  (
  `id_tipo_equipo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_tipo_equipo`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tipo_equipo
-- ----------------------------
INSERT INTO `tipo_equipo` VALUES (1, 'Servidor', 'A');
INSERT INTO `tipo_equipo` VALUES (2, 'Pc Escritorio', 'A');
INSERT INTO `tipo_equipo` VALUES (3, 'Impresora Laser', 'A');
INSERT INTO `tipo_equipo` VALUES (4, 'Impresora / Fotocopiadora', 'A');
INSERT INTO `tipo_equipo` VALUES (5, 'Impresora InyecciÃ³n a Tinta', 'A');
INSERT INTO `tipo_equipo` VALUES (6, 'Laptop', 'A');
INSERT INTO `tipo_equipo` VALUES (7, 'Impresora Multifuncional', 'A');
INSERT INTO `tipo_equipo` VALUES (8, 'EscÃ¡ner Capturador de imagen', 'A');
INSERT INTO `tipo_equipo` VALUES (9, 'Monitor', 'A');
INSERT INTO `tipo_equipo` VALUES (10, 'Impresora Tinta', 'A');
INSERT INTO `tipo_equipo` VALUES (11, 'Fotocopiadora', 'A');

-- ----------------------------
-- Table structure for tipo_horario
-- ----------------------------
DROP TABLE IF EXISTS `tipo_horario`;
CREATE TABLE `tipo_horario`  (
  `tipo_horario_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_horario_descripcion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tipo_horario_estado` int(255) NULL DEFAULT 1,
  PRIMARY KEY (`tipo_horario_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tipo_horario
-- ----------------------------
INSERT INTO `tipo_horario` VALUES (1, 'ENTRADA', 1);
INSERT INTO `tipo_horario` VALUES (2, 'SALIDA', 1);

-- ----------------------------
-- Table structure for tipo_persona
-- ----------------------------
DROP TABLE IF EXISTS `tipo_persona`;
CREATE TABLE `tipo_persona`  (
  `id_tipo_persona` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `estado` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_tipo_persona`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tipo_persona
-- ----------------------------
INSERT INTO `tipo_persona` VALUES (1, 'SI', 'A');
INSERT INTO `tipo_persona` VALUES (2, 'NO', 'A');

-- ----------------------------
-- Table structure for turno
-- ----------------------------
DROP TABLE IF EXISTS `turno`;
CREATE TABLE `turno`  (
  `turno_id` int(11) NOT NULL AUTO_INCREMENT,
  `turno_descripcion` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `turno_estado` int(255) NULL DEFAULT 1,
  PRIMARY KEY (`turno_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of turno
-- ----------------------------
INSERT INTO `turno` VALUES (1, 'MAÑANA', 1);
INSERT INTO `turno` VALUES (2, 'TARDE', 1);

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario`  (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `usuario` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `clave` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `estado` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_usuario`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO `usuario` VALUES (1, 'Administrador', 'admin', 'admin', 'A');

SET FOREIGN_KEY_CHECKS = 1;
