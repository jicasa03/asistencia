<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
    <tr>
        <th width="8%" class="text-center">Item</th>
        <th width="30%">Nnmbres y Apellidos</th>
        <th width="30%">Área</th>
        <th width="10%">Feje de Área</th>
        <th width="22%" class="text-center">Acción</th>
    </tr>
    </thead>
    <tbody>
    <?php $i=0; foreach ($personal as $value) { $i++; ?>
        <tr class="odd gradeA">
            <td class="text-center"><?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?></td>
            <td><?php echo $value['nombres'];?></td>
            <td><?php echo $value['area'];?></td>
            <td class="text-center"><?php echo $value['jefe'];?></td>
            <td class="text-center">
                <?php if ($value['estado'] == "A") {?>
                    <button class="btn btn-warning btn-xs" onclick="editar_personal(<?php echo $value['id_persona'];?>)"><i class="fa fa-edit"></i> Editar</button>
                    <button class="btn btn-danger btn-xs" onclick="confirmar_anular(<?php echo $value['id_persona'];?>)"><i class="fa fa-trash-o"></i> Anular</button>
                <?php } else {?>
                    <button class="btn btn-warning btn-xs" onclick="confirmar_restablecer(<?php echo $value['id_persona'];?>)"><i class="fa fa-edit"></i> Restablecer</button>
                    <button class="btn btn-danger btn-xs" onclick="confirmar_eliminar(<?php echo $value['id_persona'];?>)"><i class="fa fa-trash-o"></i> Eliminar</button>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>