<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
    <tr>
        <th width="10%" class="text-center">Item</th>
        <th width="45%">Nombres y Apellidos</th>
        <th width="25%">Usuario</th>
        <th width="20%" class="text-center">Acción</th>
    </tr>
    </thead>
    <tbody>
    <?php $i=0; foreach ($usuarios as $value) { $i++; ?>
        <tr class="odd gradeA">
            <td class="text-center"><?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?></td>
            <td><?php echo $value['nombres'];?></td>
            <td><?php echo $value['usuario'];?></td>
            <td class="text-center">
                <?php if ($value['estado'] == "A") {?>
                    <button class="btn btn-warning btn-xs" onclick="editar_usuario(<?php echo $value['id_usuario'];?>)"><i class="fa fa-edit"></i> Editar</button>
                    <button class="btn btn-danger btn-xs" onclick="confirmar_anular(<?php echo $value['id_usuario'];?>)"><i class="fa fa-trash-o"></i> Anular</button>
                <?php } else {?>
                    <button class="btn btn-warning btn-xs" onclick="confirmar_restablecer(<?php echo $value['id_usuario'];?>)"><i class="fa fa-edit"></i> Restablecer</button>
                    <button class="btn btn-danger btn-xs" onclick="confirmar_eliminar(<?php echo $value['id_usuario'];?>)"><i class="fa fa-trash-o"></i> Eliminar</button>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>