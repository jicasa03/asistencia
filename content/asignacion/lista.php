<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
    <tr>
        <th width="5%" class="text-center">Item</th>
        <th width="35%">Responsable</th>
        <th width="40%">Descripción de Equipo</th>
        <th width="20%" class="text-center">Acción</th>
    </tr>
    </thead>
    <tbody>
    <?php $i=0; foreach ($asignacion as $value) { $i++; ?>
        <tr class="odd gradeA">
            <td class="text-center"><?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?></td>
            <td><?php echo $value['nombres'];?></td>
            <td><?php echo $value['marca'].' - '.$value['modelo'].' '.$value['ip_equipo'];?></td>
            <td class="text-center">
                <button class="btn btn-warning btn-xs" onclick="editar_asignacion(<?php echo $value['id_equipo_persona'];?>)"><i class="fa fa-pencil"></i> Editar</button>
                <!--<button class="btn btn-danger btn-xs" onclick="confirmar_anular(<?php echo $value['id_equipo_persona'];?>)"><i class="fa fa-times"></i> Anular</button>-->
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>