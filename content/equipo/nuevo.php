<div class="card-title">
    <div class="title"><h4>Datos Básicos</h4></div>
</div>
<div class="panel-body">
    <form class="form-horizontal" id="form_equipo">
        <div class="form-group">
            <input type="hidden" name="equipo_id" id="equipo_id" value="<?php if (isset($equipo)){ echo $equipo[0]['id_equipo'];}else{ echo "-1";}?>">
            <label for="inputEmail3" class="col-sm-2 control-label">Tipo Equipo</label>
            <div class="col-sm-4">
                <select class="form-control" name="id_tipo_equipo" id="id_tipo_equipo">
                    <option value="">:: SELECCIONE ::</option>
                    <?php foreach ($tipo_equipo as $value){
                        if (isset($equipo)){ if($equipo[0]['id_tipo_equipo'] == $value['id_tipo_equipo']){
                            $select = "selected";
                        } else { $select = ""; } ?>
                        <option value="<?php echo $value['id_tipo_equipo']?>" <?php echo $select;?>><?php echo $value['descripcion'];?></option>
                        <?php } else {
                        ?>
                        <option value="<?php echo $value['id_tipo_equipo']?>"><?php echo $value['descripcion'];?></option>
                    <?php }} ?>
                </select>
            </div>
            <label for="inputEmail3" class="col-sm-2 control-label">Marca</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="marca" id="marca" placeholder="Marca" value="<?php if (isset($equipo)){ echo $equipo[0]['marca'];}?>">
            </div>
        </div>

        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Modelo</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="modelo" id="modelo" placeholder="Modelo" value="<?php if (isset($equipo)){ echo $equipo[0]['modelo'];}?>">
            </div>
            <label for="inputEmail3" class="col-sm-2 control-label">Usuario</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="usuario" id="usuario" placeholder="Usuario" value="<?php if (isset($equipo)){ echo $equipo[0]['usuario'];}?>">
            </div>
        </div>

        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">IP de Equipo</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="ip_equipo" id="ip_equipo" placeholder="IP" value="<?php if (isset($equipo)){ echo $equipo[0]['ip_equipo'];}?>">
            </div>
            <label for="inputEmail2" class="col-sm-2 control-label">Estado</label>
            <div class="col-sm-4">
                <select class="form-control" name="id_estado" id="id_estado">
                    <option value="">:: SELECCIONE ::</option>
                    <?php foreach ($estado as $value){
                        if (isset($equipo)){ if($equipo[0]['id_estado'] == $value['id_estado']){
                            $select = "selected";
                        } else { $select = ""; } ?>
                            <option value="<?php echo $value['id_estado'];?>" <?php echo $select;?>><?php echo $value['descripcion'];?></option>
                        <?php } else {
                            ?>
                            <option value="<?php echo $value['id_estado'];?>"><?php echo $value['descripcion'];?></option>
                        <?php }} ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Características</label>
            <div class="col-sm-9">
                <textarea class="form-control" rows="3" name="caracteristicas" id="caracteristicas"><?php if (isset($equipo)){ echo $equipo[0]['caracteristicas'];}?></textarea>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="col-sm-offset-5 col-sm-5">
                <button type="button" class="btn btn-danger" id="cancelar"><i class="fa fa-times"></i> Cerrar</button>
                <button type="button" class="btn btn-success" id="guardar_equipo"><i class="fa fa-save"></i> Guardar</button>
            </div>
        </div>
    </form>
</div>


<script src="app/equipo.js"></script>