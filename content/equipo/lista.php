<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
    <tr>
        <th width="1%" class="text-center">Item</th>
        <th width="16%" class="text-center">T. Equipo</th>
        <th width="23%">Descripción</th>
        <th width="30%" class="text-center">IP</th>
        <th width="10%" class="text-center">Estado</th>
        <th width="20%" class="text-center">Acción</th>
    </tr>
    </thead>
    <tbody>
    <?php $i=0; foreach ($equipo as $value) { $i++; ?>
        <tr class="odd gradeA">
            <td class="text-center"><?php echo str_pad($i, 2, "0", STR_PAD_LEFT); ?></td>
            <td><?php echo $value['tipo_equipo'];?></td>
            <td><?php echo $value['marca'].' - '.$value['modelo'];?></td>
            <td class="text-center"><?php echo $value['ip_equipo'];?></td>
            <td class="text-center"><?php echo $value['estado'];?></td>
            <td class="text-center">
                <?php if ($value['id_estado'] == 1) {?>
                    <button class="btn btn-warning btn-xs" onclick="editar_equipo(<?php echo $value['id_equipo'];?>)"><i class="fa fa-edit"></i> Editar</button>
                    <button class="btn btn-danger btn-xs" onclick="confirmar_anular(<?php echo $value['id_equipo'];?>)"><i class="fa fa-trash-o"></i> Anular</button>
                <?php } else {?>
                    <button class="btn btn-warning btn-xs" onclick="confirmar_restablecer(<?php echo $value['id_equipo'];?>)"><i class="fa fa-edit"></i> Restablecer</button>
                    <button class="btn btn-danger btn-xs" onclick="confirmar_eliminar(<?php echo $value['id_equipo'];?>)"><i class="fa fa-trash-o"></i> Eliminar</button>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>