<?php include('cabecera.php');
require 'conexion.php';
$query1=$conexion->prepare("SELECT * FROM tipo_equipo WHERE estado = 'A' ");
$query1->execute();
$tipo_equipo = $query1->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="header">
    <h3 class="page-header"><b id="titulo_pagina">Lista de Equipos</b></h3>
</div>
<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading right botones show">
                    <button class="btn btn-success" id="nuevo_equipo"><i class="fa fa-plus"></i> NUEVO</button>
                    <button class="btn btn-primary" id="operativos"><i class="fa fa-check"></i> OPERATIVOS</button>
                    <button class="btn btn-warning" id="inoperativos"><i class="fa fa-warning"></i> INOPERATIVOS</button>
                    <button class="btn btn-secondary" id="desfasado"><i class="fa fa-times"></i> DESFASADO</button>
                    <button class="btn btn-danger" id="de_baja"><i class="fa fa-times"></i> DE BAJA</button>
                    <button class="btn btn-info"><i class="fa fa-file-o"></i> REPORTE</button>
                    <button class="btn btn-default"><i class="fa fa-print"></i> IMPRIMIR</button>
                </div>
                <div class="panel-body">
                    <div id="content"></div>
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>
</div>

<!-- MODAL CONFIRMACION-->
<div class="modal fade" id="modal_confirmacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header btn-danger">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center" id="titulo"><b><i class="fa fa-warning"></i> Advertencia</b></h4>
            </div>
            <form class="form-horizontal" id="form_area">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="modal-title text-center" id="content_confirmacion"><b></b></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-times"></i> No, Cerrar!</button>
                    <button type="button" class="btn btn-success" onclick="eliminar_area()"><i class="fa fa-check"></i> Si, Seguro!</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>
<script>
    $(document).ready(function () {
        $(".mantenimiento").addClass("active-menu");
        $(".mantenimiento_menu").addClass("in");
        $(".equipo").addClass("menu-select");

        listado(1);
        $("#operativos").click(function(e) {
            listado(1);
        });

        $("#inoperativos").click(function(e) {
            listado(2);
        });
        $("#desfasado").click(function(e) {
            listado(3);
        });
        $("#de_baja").click(function(e) {
            listado(4);
        });
    });
</script>
<script src="app/equipo.js"></script>
