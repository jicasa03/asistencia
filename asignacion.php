﻿<?php include('cabecera.php');
$query1=$conexion->prepare("SELECT * FROM persona WHERE estado = 'A' ");
$query1->execute();
$personal = $query1->fetchAll(PDO::FETCH_ASSOC);

$query=$conexion->prepare("SELECT eq.*,tp.descripcion as tipo_equipo,es.descripcion as estado 
                                    FROM equipo eq INNER JOIN tipo_equipo tp ON (tp.id_tipo_equipo = eq.id_tipo_equipo)
                                    INNER JOIN estado es ON (es.id_estado = eq.id_estado) ");
$query->execute();
$equipo = $query->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="header">
    <h3 class="page-header"><b>Lista de Asignación de Equipos</b></h3>
</div>
<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading right">
                    <button class="btn btn-success" id="nueva_asignacion"><i class="fa fa-plus"></i> ASIGNAR</button>
                    <a href="reportes/area.php" class="btn btn-info"><i class="fa fa-file-o"></i> REPORTE</a>
                    <!--<button class="btn btn-default" onclick="javascript:printDiv('content')"><i class="fa fa-print"></i> IMPRIMIR</button>-->
                    <a href="imprimir/asignacion.php" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> REPORTE</a>
                </div>
                <div class="panel-body">
                    <div id="content"></div>
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>
</div>

<!-- MODAL REGISTRAR-->
<div class="modal fade" id="modal_asignacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="titulo"><b>Registar Asignaciíon de Equipo</b></h4>
            </div>
            <form class="form-horizontal" id="form_asignacion">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <input type="hidden" name="asignacion_id" id="asignacion_id" value="-1">
                                <label for="inputEmail3" class="col-sm-3 control-label">Responsable</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="id_personal" id="id_personal">
                                        <option value="">:: SELECCIONE ::</option>
                                        <?php foreach ($personal as $value){ ?>
                                            <option value="<?php echo $value['id_persona']?>"><?php echo $value['nombres'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Equipo</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="id_equipo" id="id_equipo">
                                        <option value="">:: SELECCIONE ::</option>
                                        <?php foreach ($equipo as $value){ ?>
                                            <option value="<?php echo $value['id_equipo']?>"><?php echo $value['tipo_equipo']." - ".$value['marca']." ".$value['modelo']." - ".$value['ip_equipo'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                    <button type="button" class="btn btn-success" id="guardar_asignacion"><i class="fa fa-save"></i> Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- MODAL CONFIRMACION-->
<div class="modal fade" id="modal_confirmacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header btn-danger">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center" id="titulo"><b><i class="fa fa-warning"></i> Advertencia</b></h4>
            </div>
            <form class="form-horizontal" id="form_area">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="modal-title text-center" id="content_confirmacion"><b></b></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-times"></i> No, Cerrar!</button>
                    <button type="button" class="btn btn-success" onclick="eliminar_area()"><i class="fa fa-check"></i> Si, Seguro!</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>
<script>
    $(document).ready(function () {
        $(".mantenimiento").addClass("active-menu");
        $(".mantenimiento_menu").addClass("in");
        $(".asignacion").addClass("menu-select");
        listado();
    });

    function printDiv(content) {
        var contenido= document.getElementById(content).innerHTML;
        var contenidoOriginal= document.body.innerHTML;
        document.body.innerHTML = contenido;
        window.print();
        document.body.innerHTML = contenidoOriginal;
    }
</script>
<script src="app/asignacion.js"></script>
