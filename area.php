<?php include('cabecera.php');
?>
    <div class="header">
        <h3 class="page-header"><b>Lista de Áreas</b></h3>
    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading right">
                        <button class="btn btn-success" id="nueva_area"><i class="fa fa-plus"></i> NUEVA</button>
                        <button class="btn btn-primary" id="areas_activas"><i class="fa fa-check"></i> ACTIVAS</button>
                        <button class="btn btn-danger" id="areas_anuladas"><i class="fa fa-times"></i> ANULADAS</button>
                        <a href="reportes/area.php" class="btn btn-info"><i class="fa fa-file-o"></i> REPORTE</a>
                        <a href="imprimir/area.php" class="btn btn-default" target="_blank"><i class="fa fa-print"></i> IMPRIMIR</a>
                    </div>
                    <div class="panel-body">
                        <div id="content"></div>
                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>

    <!-- MODAL REGISTRAR-->
    <div class="modal fade" id="modal_area" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="titulo"><b>Registar Nueva Área</b></h4>
                </div>
                <form class="form-horizontal" id="form_area">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <input type="hidden" name="area_id" id="area_id" value="-1">
                                    <label for="inputEmail3" class="col-sm-3 control-label">Descripción</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" name="descripcion" id="descripcion" placeholder="Descripción">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                        <button type="button" class="btn btn-success" id="guardar_area"><i class="fa fa-save"></i> Guardar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- MODAL CONFIRMACION-->
    <div class="modal fade" id="modal_confirmacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title text-center" id="titulo"><b><i class="fa fa-warning"></i> Advertencia</b></h4>
                </div>
                <form class="form-horizontal" id="form_area">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4 class="modal-title text-center" id="content_confirmacion"><b></b></h4>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-times"></i> No, Cerrar!</button>
                        <button type="button" class="btn btn-success" onclick="eliminar_area()"><i class="fa fa-check"></i> Si, Seguro!</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php include('footer.php'); ?>
<script>
    $(document).ready(function () {
        $(".mantenimiento").addClass("active-menu");
        $(".mantenimiento_menu").addClass("in");
        $(".area").addClass("menu-select");

        listado("A");
        $("#areas_activas").click(function(e) {
            listado("A");
        });

        $("#areas_anuladas").click(function(e) {
            listado("I");
        });
    });
</script>
<script src="app/area.js"></script>
