<?php include('cabecera.php');


$query1=$conexion->prepare("SELECT * FROM turno WHERE turno_estado = 1");
$query1->execute();
$turno = $query1->fetchAll(PDO::FETCH_ASSOC);

$query2=$conexion->prepare("SELECT * FROM tipo_horario WHERE tipo_horario_estado = 1");
$query2->execute();
$tipo_horario = $query2->fetchAll(PDO::FETCH_ASSOC);

?>


<style type="text/css">
	.clock {
  

  
    color: #0d47a1;
    font-size: 40px;
    font-family: Orbitron;
    letter-spacing: 7px;
   
    margin-top: 100px;

}

.texto{
    font-size: 10px;
}
</style>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>












<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css">





<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>









<div class="header">
    <h3 class="page-header"><b>Reporte de asistencia</b></h3>
</div>
<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
                <div class="panel-heading right">
                   
                <div class="panel-body">

                    <div class="row">
                        <form id="formulario" onsubmit="return generar_tabla()">
                        <div class="col-md-3">
                            <input type="hidden" value="2" name="accion">
                            <div class="form-group">
                                <label>Fecha de inicio</label>
                                <input type="date" value="<?php echo date('Y-m-d') ?>" id="fecha_inicio" class="form-control" name="fecha_inicio">
                            </div>
                        </div>
                         <div class="col-md-3">
                            <div class="form-group">
                                <label>Fecha de fin</label>
                                <input type="date" value="<?php echo date('Y-m-d') ?>" id="fecha_fin" class="form-control" name="fecha_fin">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" style="margin-top: 25px;" class="btn btn-primary">Mostrar</button>
                        </div>
                    </form>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                            <table id="tabla" class="table table-bordered">
                                <thead>
                                    <tr id="cabecera_arriba">
                                       <td  class="text-center"  rowspan="2"><label style="width: 100px;" class="texto">Nombre y Apellido</label></td> 
                                        



                                     <!--   <td colspan="4" class="text-center"> <label class="texto">10/10/2012</label></td> -->
                                      

                                    </tr>
                                  <tr id="cabecera_abajo">
                                        <!--<td class="text-center"><label class="texto">En. Mañana</label></td> 
                                        <td class="text-center" ><label class="texto">Sal. Mañana</label></td> 
                                        <td class="text-center"><label class="texto">En. Tarde</label></td> 
                                        <td class="text-center"><label class="texto">Sal. Tarde</label></td>--> 

                                     

                                    </tr>



                                </thead>
                                <tbody>
                                   

                                </tbody>
                            </table>

</div>


                        </div>
                    </div>
                    <!-- <div class="row">
                     	<div class="col-md-6">
                     		<div class="text-center">

                     		<div id="MyClockDisplay" class="clock" onload="showTime()"></div>
                     	</div>

                     	</div>
                     	<div class="col-md-6">
                     			<form id="form_usuario" onsubmit="return registrar_asistencia()">
                     		<div class="row">
                     			<div class="col-md-9">
                     			<div class="form-group">
                     				<label>INGRESAR DNI</label>
                     				<input type="text" required="true" autocomplete="off" autofocus="true" maxlength="8" name="documento" id="documento" class="form-control">
                     			</div>
                     		</div>
                     		<div class="col-md-3">

                     			<button style="margin-top:25px;" id="guardar_usuario" class="btn btn-primary">ingresar</button>
                     		</div>
                      		</div>
                     		<div class="row">
                     			<div class="col-md-6">
                     				<label>Turno</label>
                     				<select class="form-control" id="turno" name="turno">
                     					 <?php foreach ($turno as $value){ ?>
                                            <option value="<?php echo $value['turno_id']?>"><?php echo $value['turno_descripcion'];?></option>
                                        <?php } ?>
                     				</select>
                     			</div>
                     			<div class="col-md-6">
                     				<label>Tipo de asistencia</label>
                     				<select class="form-control" id="turno" name="turno">
                     					 <?php foreach ($tipo_horario as $value){ ?>
                                            <option value="<?php echo $value['tipo_horario_id']?>"><?php echo $value['tipo_horario_descripcion'];?></option>
                                        <?php } ?>
                     				</select>
                     			</div>
                     		</div>

                     	</form>

                     		<div class="row">
                     			<div class="text-center">
                     				<br>
                     			<label style="font-size: 30px;margin-bottom: 20px;">RESPUESTA</label>
                     		</div>
                     		</div>
                     		<div class="row">
                     			<div class="col-md-6">
                     				<label style="margin-bottom: 20px;font-weight: bold;">NOMBRE COMPLETO:</label>
                     			</div>
                     			<div class="col-md-6">
                     				<label  style="margin-bottom: 20px;font-size: 15px;" id="nombre">no se encuentra datos</label>
                     			</div>
                     		</div>
                     			<div class="row">
                     			<div class="col-md-6">
                     				<label  style="margin-bottom: 20px;font-weight: bold;">FECHA DE REGISTRO:</label>
                     			</div>
                     			<div class="col-md-6">
                     				<label  style="margin-bottom: 20px;font-size: 15px;" id="nombre">no se encuentra datos</label>
                     			</div>
                     		</div>
                     			<div class="row">
                     			<div class="col-md-6">
                     				<label  style="margin-bottom: 20px;font-weight: bold;">ESTADO DE CARGA:</label>
                     			</div>
                     			<div class="col-md-6">
                     				<label  style="margin-bottom: 20px;font-size: 15px;" id="nombre">no se encuentra datos</label>
                     			</div>
                     		</div>
                     	</div>
                     </div>
                </div>-->
            </div>

        </div>
    </div>
</div>
</div>

<?php include('footer.php'); ?>

<script type="text/javascript">


function generar_tabla(){
   
   $.post("control/asistencia.php",$("#formulario").serialize(),function(data){


       $("#cabecera_arriba").append(data["cabecera_arriba"]);
       $("#cabecera_abajo").append(data["cabecera_abajo"]);







         $.extend( $.fn.dataTable.defaults, {

    
  

        language: {

       

            lengthMenu: '<span>Mostrar:</span> _MENU_',

            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }

        }

    });








  var table=$('#tabla').DataTable({
 dom: 'Bfrtip',
            "oLanguage":{
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
     fixedHeader: true,
"scrollX": true,


            buttons: [

             

                'excelHtml5',

             

            ]

 

    });





      $.extend( true, $.fn.dataTable.defaults, {

    buttons: [  ]

} );























   },"json");
    return false;
}




/*

	function registrar_asistencia(){

           document.getElementById('guardar_usuario').disabled=true;
    $.ajax({
        url:'control/asistencia.php',
        data:'accion=1&'+$("#form_usuario").serialize(),
        type:'post',
         dataType: "json",
        success: function(data) {
       

        }
    });
    return false;

	}

	function showTime(){
    var date = new Date();
    var h = date.getHours(); // 0 - 23
    var m = date.getMinutes(); // 0 - 59
    var s = date.getSeconds(); // 0 - 59
    var session = "AM";
    
    if(h == 0){
        h = 12;
    }
    
    if(h > 12){
        h = h - 12;
        session = "PM";
    }
    
    h = (h < 10) ? "0" + h : h;
    m = (m < 10) ? "0" + m : m;
    s = (s < 10) ? "0" + s : s;
    
    var time = h + ":" + m + ":" + s + " " + session;
    document.getElementById("MyClockDisplay").innerText = time;
    document.getElementById("MyClockDisplay").textContent = time;
    
    setTimeout(showTime, 1000);
    
}

showTime();
	$(document).ready(function () {



$(".asistencia").addClass("active-menu");
        $(".asistencia_menu").addClass("in");
        $(".asistencia").addClass("menu-select");













    $("#nueva_area").click(function(e) {
        e.preventDefault();
        document.getElementById("form_area").reset();
        $("#area_id").val(-1);
        $("#titulo").html('Registrar Area');
        document.getElementById('guardar_area').disabled=false;
        $('#modal_area').modal('show');
    });
    $('#modal_area').on('shown.bs.modal', function(e) {
        e.preventDefault();
        $("#descripcion").focus();
    });

    $("#guardar_area").click(function () {
        if ($('#descripcion').val().trim() === '') {
            $('#descripcion').val("").focus();
            return false;
        }

        if ($("#area_id").val() == -1){
            var valor = 1;
        }else{
            valor = 2;
        }
        guardar(valor);
    });
});*/
</script>