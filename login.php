<?php
session_start();
require('conexion.php');
if(!empty($_POST))
{
    $error = '';

    $query=$conexion->prepare("SELECT * FROM usuario WHERE usuario = '{$_POST["usuario"]}' AND clave = '{$_POST["clave"]}'");
    $query->execute();
    $user=$query->fetchAll(PDO::FETCH_ASSOC);
    if(count($user) == 1) {
        $_SESSION['usuario'] = $user[0]['nombres'];
        header("location: principal.php");
    } else {
        $error = "El nombre o contraseña son incorrectos";
    }
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta content="" name="description" />
    <meta content="webthemez" name="author" />
    <title>Login - Sistema de control de equipos informáticos</title>
    <!-- Bootstrap Styles-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/login.css" rel="stylesheet" />
</head>

<body style="background: #f5f5f5;">
    <div class="wrapper fadeInDown">
        <div id="formContent">
            <!-- Tabs Titles -->
            <h2 class="active">Iniciar Sesión </h2>

            <!-- Icon -->
            <div class="first"> <!-- fadeIn -->
                <img src="assets/inicio.PNG" id="icon" alt="User Icon" />
            </div>

            <!-- Login Form -->
            <form action="" id="form_login" method="post">
                <input type="text" class="fadeIn second" name="usuario" id="usuario" required placeholder="Usuario">
                <input type="password" class="fadeIn third" name="clave" id="clave" required placeholder="Contraseña">
                <input type="submit" class="fadeIn fourth" value="Entrar">

            </form>
<div class="row">
             <a href="asistir.php"  class="fadeIn fourth">Asistencia</a>
</div>
            <div id="formFooter">
                <label style="color: red;"><?php if (isset($error)){echo $error;}?></label>
            </div>

        </div>
    </div>
</body>
<script src="assets/js/jquery-1.10.2.js"></script>

<script>
    $(document).ready(function () {
        $('#usuario').val("").focus();
    });
</script>
</html>