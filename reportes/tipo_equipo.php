<?php
/** Incluye PHPExcel */
include_once ("../assets/phpexcel/PHPExcel.php");
require_once '../conexion.php';
$query=$conexion->prepare("SELECT * FROM tipo_equipo WHERE estado = 'A' ");
$query->execute();
$tipo_equipo = $query->fetchAll(PDO::FETCH_ASSOC);

// Crear nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();
// Propiedades del documento
$objPHPExcel->getProperties()->setCreator("Obed Alvarado")
    ->setLastModifiedBy("Obed Alvarado")
    ->setTitle("Office 2010 XLSX Documento de prueba")
    ->setSubject("Office 2010 XLSX Documento de prueba")
    ->setDescription("Documento de prueba para Office 2010 XLSX, generado usando clases de PHP.")
    ->setKeywords("office 2010 openxml php")
    ->setCategory("Archivo con resultado de prueba");
// Combino las celdas desde A1 hasta E1
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:B1');

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'REPORTE DE TIPOS DE EQUIPOS')
    ->setCellValue('A2', 'ITEM')
    ->setCellValue('B2', 'DESCRIPCIÓN');

// Fuente de la primera fila en negrita
$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$boldArray_1 = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$estilo = array('borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

$objPHPExcel->getActiveSheet()->getStyle('A1:B1')->applyFromArray($boldArray);
$objPHPExcel->getActiveSheet()->getStyle('A2:A2')->applyFromArray($boldArray_1);
$objPHPExcel->getActiveSheet()->getStyle('B2:B2')->applyFromArray($boldArray_1);

$objPHPExcel->getActiveSheet()->getStyle('A2:A2')->applyFromArray($estilo);
$objPHPExcel->getActiveSheet()->getStyle('B2:B2')->applyFromArray($estilo);

//Ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(8);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);

/*Extraer datos de MYSQL*/
$cel=3;//Numero de fila donde empezara a crear  el reporte
$item = 1;
$data_count = count($tipo_equipo);
for ($i = 0; $i < $data_count; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A".$cel, "   ".str_pad($item, 2, "0", STR_PAD_LEFT))
        ->setCellValue("B".$cel, "   ".$tipo_equipo[$i]['descripcion']);
    $cel++;
    $item++;
    $objPHPExcel->getActiveSheet()->getStyle('A2:A'.($cel-1))->applyFromArray($estilo);
    $objPHPExcel->getActiveSheet()->getStyle('B2:B'.($cel-1))->applyFromArray($estilo);
}

// estilos
$boldArray_1 = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('A3:A'.$cel)->applyFromArray($boldArray_1);

// Establecer índice de hoja activa a la primera hoja , por lo que Excel abre esto como la primera hoja
$objPHPExcel->setActiveSheetIndex(0);
// Redirigir la salida al navegador web de un cliente ( Excel5 )
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Reporte de Tipos de Equipo.xls"');
header('Cache-Control: max-age=0');
// Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
header('Cache-Control: max-age=1');
// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;