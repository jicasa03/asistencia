<?php
/** Incluye PHPExcel */
include_once ("../assets/phpexcel/PHPExcel.php");
require_once '../conexion.php';
$query=$conexion->prepare("SELECT per.*, ar.descripcion as area,tp.descripcion as jefe FROM persona per 
                                  INNER JOIN area ar ON (per.id_area=ar.id_area)
                                  INNER JOIN tipo_persona tp ON (per.id_tipo_persona=tp.id_tipo_persona)
                                  WHERE per.estado = 'A' ");
$query->execute();
$personal = $query->fetchAll(PDO::FETCH_ASSOC);

// Crear nuevo objeto PHPExcel
$objPHPExcel = new PHPExcel();
// Propiedades del documento
$objPHPExcel->getProperties()->setCreator("dh28")
    ->setLastModifiedBy("Obed Alvarado")
    ->setTitle("Office 2010 XLSX Documento de prueba")
    ->setSubject("Office 2010 XLSX Documento de prueba")
    ->setDescription("Documento de prueba para Office 2010 XLSX, generado usando clases de PHP.")
    ->setKeywords("office 2010 openxml php")
    ->setCategory("Archivo con resultado de prueba");
// Combino las celdas desde A1 hasta E1
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1');

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'REPORTE DE PERSONAL')
    ->setCellValue('A2', 'ITEM')
    ->setCellValue('B2', 'NOMBRES Y APELLIDOS')
    ->setCellValue('C2', 'ÁREA')
    ->setCellValue('D2', 'JEFE');

// Fuente de la primera fila en negrita
$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$boldArray_1 = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$estilo = array('borders' => array('outline' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

$objPHPExcel->getActiveSheet()->getStyle('A1:B1')->applyFromArray($boldArray);
$objPHPExcel->getActiveSheet()->getStyle('A2:A2')->applyFromArray($boldArray_1);
$objPHPExcel->getActiveSheet()->getStyle('B2:B2')->applyFromArray($boldArray_1);
$objPHPExcel->getActiveSheet()->getStyle('C2:C2')->applyFromArray($boldArray_1);
$objPHPExcel->getActiveSheet()->getStyle('D2:D2')->applyFromArray($boldArray_1);

$objPHPExcel->getActiveSheet()->getStyle('A2:A2')->applyFromArray($estilo);
$objPHPExcel->getActiveSheet()->getStyle('B2:B2')->applyFromArray($estilo);
$objPHPExcel->getActiveSheet()->getStyle('C2:C2')->applyFromArray($estilo);
$objPHPExcel->getActiveSheet()->getStyle('D2:D2')->applyFromArray($estilo);

//Ancho de las columnas
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);

/*Extraer datos de MYSQL*/
$cel=3;//Numero de fila donde empezara a crear  el reporte
$item = 1;
$data_count = count($personal);
for ($i = 0; $i < $data_count; $i++) {
    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue("A".$cel, "   ".str_pad($item, 2, "0", STR_PAD_LEFT))
        ->setCellValue("B".$cel, "   ".$personal[$i]['nombres'])
        ->setCellValue("C".$cel, "   ".$personal[$i]['area'])
        ->setCellValue("D".$cel, "   ".$personal[$i]['jefe']);
    $cel++;
    $item++;
    $objPHPExcel->getActiveSheet()->getStyle('A2:A'.($cel-1))->applyFromArray($estilo);
    $objPHPExcel->getActiveSheet()->getStyle('B2:B'.($cel-1))->applyFromArray($estilo);
    $objPHPExcel->getActiveSheet()->getStyle('C2:C'.($cel-1))->applyFromArray($estilo);
    $objPHPExcel->getActiveSheet()->getStyle('D2:D'.($cel-1))->applyFromArray($estilo);
    $objPHPExcel->getActiveSheet()->getStyle('A2:A'.($cel-1))->applyFromArray($boldArray_1);
    $objPHPExcel->getActiveSheet()->getStyle('D2:D'.($cel-1))->applyFromArray($boldArray_1);

}

// estilos
$boldArray_1 = array('alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('A3:A'.$cel)->applyFromArray($boldArray_1);

// Establecer índice de hoja activa a la primera hoja , por lo que Excel abre esto como la primera hoja
$objPHPExcel->setActiveSheetIndex(0);
// Redirigir la salida al navegador web de un cliente ( Excel5 )
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Reporte de Personal.xls"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;