<?php
session_start();
require 'conexion.php';
if (!isset($_SESSION['usuario'])){
    //require_once("principal.php");
    header("location: login.php");

}

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta content="" name="description" />
    <meta content="webthemez" name="author" />
    <title>Sistema de control de equipos informáticos</title>
    <!-- Bootstrap Styles-->
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">
    <link href="assets/css/checkbox3.min.css" rel="stylesheet" >
    <!-- FontAwesome Styles-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="assets/css/custom-styles.css" rel="stylesheet" />
    <link href="assets/css/alertify.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <!-- TABLE STYLES-->
    <link href="assets/css/select2.min.css" rel="stylesheet" >










<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>







<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.0/css/buttons.dataTables.min.css">





<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.0/js/buttons.html5.min.js"></script>










</head>

<body>
<div id="wrapper">
    <nav class="navbar navbar-default top-navbar" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./"><strong><i class="icon fa fa-table"></i> GTBM-T</strong></a>

            <div id="sideNav" href="#">
                <i class="fa fa-bars icon"></i>
            </div>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-user fa-fw"></i><b><?php echo $_SESSION['usuario'];?> </b><i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i> Perfil</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Configuración</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="salir.php"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
    </nav>
    <!--/. NAV TOP  -->
    <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <a class="mantenimiento" href="#"><i class="fa fa-gear"></i> Mantenimiento<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level mantenimiento_menu">
                        <li class="personal ">
                            <a href="personal.php"><i class="fa fa-user"></i> Personal</a>
                        </li>
                        <li class="area ">
                            <a href="area.php"><i class="fa fa-table"></i>Area</a>
                        </li>
                        <li class="tipo_equipo ">
                            <a href="tipo_equipo.php"><i class="fa fa-list"></i>Tipo Equipo</a>
                        </li>
                        <li class="equipo">
                            <a href="equipo.php"><i class="fa fa-desktop"></i>Equipo</a>
                        </li>
                        <li class="asignacion">
                            <a href="asignacion.php"><i class="fa fa-users"></i>Asignación Equipo</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a class="seguridad" href="#"><i class="fa fa-sitemap"></i> Seguridad<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level seguridad_menu">
                        <li class="usuarios">
                            <a href="usuarios.php"><i class="fa fa-user"></i>Usuarios</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a class="asistencia" href="#"><i class="fa fa-sitemap"></i> Asistencia<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level asistencia_menu">
                        <li class="control">
                            <a href="asistencia.php"><i class="fa fa-user"></i>Control</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- /. NAV SIDE  -->

    <div id="page-wrapper">