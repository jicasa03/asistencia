<?php include('cabecera.php');
?>
<div class="header">
    <h3 class="page-header"><b>Lista de Usuarios</b></h3>
</div>
<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading right">
                    <button class="btn btn-success" id="nuevo_usuario"><i class="fa fa-plus"></i> NUEVO</button>
                    <button class="btn btn-primary" id="usuarios_activos"><i class="fa fa-check"></i> ACTIVOS</button>
                    <button class="btn btn-danger" id="usuarios_anulados"><i class="fa fa-times"></i> ANULADOS</button>
                    <a href="reportes/area.php" class="btn btn-info"><i class="fa fa-file-o"></i> REPORTE</a>
                    <button class="btn btn-default"><i class="fa fa-print"></i> IMPRIMIR</button>
                </div>
                <div class="panel-body">
                    <div id="content"></div>
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>
</div>

<!-- MODAL REGISTRAR-->
<div class="modal fade" id="modal_usuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="titulo"><b>Registar Nuevo Usuario</b></h4>
            </div>
            <form class="form-horizontal" id="form_usuario">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <input type="hidden" name="usuario_id" id="usuario_id" value="-1">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nombres</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="nombres" id="nombres" placeholder="Nombres y Apellidos">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Usuario</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="usuario" id="usuario" placeholder="Usuario">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Contraseña</label>
                                <div class="col-sm-5">
                                    <input type="password" class="form-control" name="clave" id="clave" placeholder="Contraseña">
                                </div>
                                <div class="col-sm-1">
                                    <div class="checkbox3 checkbox-success checkbox-inline checkbox-check checkbox-round  checkbox-light">
                                        <input type="checkbox" id="muestra_clave" name="muestra_clave">
                                        <label for="muestra_clave">
                                            Mostrar
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                    <button type="button" class="btn btn-success" id="guardar_usuario"><i class="fa fa-save"></i> Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- MODAL CONFIRMACION-->
<div class="modal fade" id="modal_confirmacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header btn-danger">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center" id="titulo"><b><i class="fa fa-warning"></i> Advertencia</b></h4>
            </div>
            <form class="form-horizontal" id="form_area">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="modal-title text-center" id="content_confirmacion"><b></b></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-times"></i> No, Cerrar!</button>
                    <button type="button" class="btn btn-success" onclick="eliminar_area()"><i class="fa fa-check"></i> Si, Seguro!</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>
<script>
    $(document).ready(function () {
        $("#clave").attr('type', $(this).is(':checked') ? 'text' : 'password');

        $(".seguridad").addClass("active-menu");
        $(".seguridad_menu").addClass("in");
        $(".usuarios").addClass("menu-select");

        listado("A");
        $("#usuarios_activos").click(function(e) {
            listado("A");
        });
        $("#usuarios_anulados").click(function(e) {
            listado("I");
        });
        $("#muestra_clave").click(function () {
            if($("#muestra_clave").is(':checked')) {
                $('#clave').prop('type', 'text');
                $("#muestra_clave").attr('checked', true);
            } else {
                $('#clave').prop('type', 'password');
                $("#muestra_clave").attr('checked', false);
            }
        });
    });
</script>
<script src="app/usuario.js"></script>
