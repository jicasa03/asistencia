<?php
require '../conexion.php';
if(isset($_POST['accion'])){

    if ($_POST['accion']==0) {
        $query=$conexion->prepare("SELECT per.*, ar.descripcion as area,tp.descripcion as jefe FROM persona per 
                                  INNER JOIN area ar ON (per.id_area=ar.id_area)
                                  INNER JOIN tipo_persona tp ON (per.id_tipo_persona=tp.id_tipo_persona)
                                  WHERE per.estado = '{$_REQUEST['estado']}' ");
        $query->execute();
        $personal = $query->fetchAll(PDO::FETCH_ASSOC);
        require_once("../content/personal/lista.php");
    }

    if ($_POST['accion']==1) {
        $query=$conexion->prepare("select * from persona where nombres = '{$_REQUEST["nombres"]}' ");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if (count($data)==0) {
            $query1 = $conexion->prepare("INSERT INTO persona(nombres,id_area,id_tipo_persona,estado,dni) VALUES (?, ?, ?, ?,?)");
            $resultado = $query1->execute(array($_REQUEST["nombres"],$_REQUEST["id_area"],$_REQUEST['id_tipo_persona'],'A',$_REQUEST["dni"]));
            echo $resultado;
        }else{
            echo "2";
        }
    }

    if ($_POST["accion"]==2) {
        $query=$conexion->prepare("select * from persona where nombres = '{$_REQUEST["nombres"]}' ");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if (count($data)==0) {
        //    echo  "hola".$_REQUEST["dni"];
            //print_r(array($_REQUEST["nombres"],$_REQUEST['id_area'],$_REQUEST["id_tipo_persona"],$_REQUEST["dni"],$_REQUEST["personal_id"]));
            $query1 = $conexion->prepare("UPDATE persona SET nombres = ?, id_area = ?, id_tipo_persona = ? , dni=? WHERE id_persona = ?");
            $resultado = $query1->execute(array($_REQUEST["nombres"],$_REQUEST['id_area'],$_REQUEST["id_tipo_persona"],$_REQUEST["dni"],$_REQUEST["personal_id"]));
            echo $resultado;
        }else{
            if (count($data)>=1) {
                $query=$conexion->prepare("select * from persona where nombres = '{$_REQUEST["nombres"]}' AND id_persona = {$_REQUEST["personal_id"]} ");
                $query->execute();
                $data1 = $query->fetchAll(PDO::FETCH_ASSOC);
                if (count($data1)==1){

                    $query1 = $conexion->prepare("UPDATE persona SET nombres = ?, id_area = ?, id_tipo_persona = ?, dni=?  WHERE id_persona = ?");
                    $resultado = $query1->execute(array($_REQUEST["nombres"],$_REQUEST['id_area'],$_REQUEST["id_tipo_persona"],$_REQUEST["dni"],$_REQUEST["personal_id"]));
                    echo $resultado;
                } else {
                    echo "2";
                }
            }else{
                echo "2";
            }
        }
    }

    if ($_POST["accion"]==3) {
        $query=$conexion->prepare("select * from area where descripcion = '{$_REQUEST['descripcion']}' ");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if (count($data)==0) {
            echo "0";
        }else{
            echo "1";
        }
    }

    if ($_POST["accion"]==4) {
        $query=$conexion->prepare("select * from persona where id_persona = '{$_REQUEST['id']}' ");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($data);
    }

    if ($_POST["accion"]==5) {
        if ($_REQUEST['tipo']=="anular"){
            $query = $conexion->prepare("UPDATE persona SET estado = ?  WHERE id_persona = ?");
            $resultado = $query->execute(array("I",$_REQUEST["id"]));
        }
        if ($_REQUEST['tipo']=="restablecer"){
            $query = $conexion->prepare("UPDATE persona SET estado = ?  WHERE id_persona = ?");
            $resultado = $query->execute(array("A",$_REQUEST["id"]));
        }
        if ($_REQUEST['tipo']=="eliminar"){
            $query = $conexion->prepare("DELETE FROM persona WHERE id_persona = ?");
            $resultado = $query->execute(array($_REQUEST["id"]));
        }
        echo $resultado;
    }
}
?>
