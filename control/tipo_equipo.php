<?php
require '../conexion.php';
if(isset($_POST['accion'])){

    if ($_POST['accion']==0) {
        $query=$conexion->prepare("SELECT * FROM tipo_equipo WHERE estado = '{$_REQUEST['estado']}' ");
        $query->execute();
        $tipo_equipo = $query->fetchAll(PDO::FETCH_ASSOC);
        require_once("../content/tipo_equipo/lista.php");
    }

    if ($_POST['accion']==1) {
        $query=$conexion->prepare("select * from tipo_equipo where descripcion = '{$_REQUEST['descripcion']}' ");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if (count($data)==0) {
            $query1 = $conexion->prepare("INSERT INTO tipo_equipo(descripcion,estado) VALUES (?, ?)");
            $resultado = $query1->execute(array($_REQUEST["descripcion"],'A'));
            echo $resultado;
        }else{
            echo "2";
        }
    }

    if ($_POST["accion"]==2) {
        $query=$conexion->prepare("select * from tipo_equipo where descripcion = '{$_REQUEST['descripcion']}' ");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if (count($data)>=1) {
            echo "2";
        }else{
            $query1 = $conexion->prepare("UPDATE tipo_equipo SET descripcion = ? WHERE id_tipo_equipo = ?");
            $resultado = $query1->execute(array($_REQUEST["descripcion"],$_REQUEST["tipo_equipo_id"]));
            echo $resultado;
        }
    }

    if ($_POST["accion"]==4) {
        $query=$conexion->prepare("select * from tipo_equipo where id_tipo_equipo = '{$_REQUEST['id']}' ");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($data);
    }

    if ($_POST["accion"]==5) {
        if ($_REQUEST['tipo']=="anular"){
            $query = $conexion->prepare("UPDATE tipo_equipo SET estado = ?  WHERE id_tipo_equipo = ?");
            $resultado = $query->execute(array("I",$_REQUEST["id"]));
        }
        if ($_REQUEST['tipo']=="restablecer"){
            $query = $conexion->prepare("UPDATE tipo_equipo SET estado = ?  WHERE id_tipo_equipo = ?");
            $resultado = $query->execute(array("A",$_REQUEST["id"]));
        }
        if ($_REQUEST['tipo']=="eliminar"){
            $query = $conexion->prepare("DELETE FROM tipo_equipo WHERE id_tipo_equipo = ?");
            $resultado = $query->execute(array($_REQUEST["id"]));
        }
        echo $resultado;
    }
}
?>
