<?php
require '../conexion.php';
if(isset($_POST['accion'])){

    if ($_POST['accion']==0) {
        $query=$conexion->prepare("SELECT ep.*,per.*,eq.* FROM equipo_persona ep 
                                  INNER JOIN persona per ON (ep.id_persona=per.id_persona)
                                  INNER JOIN equipo eq ON (ep.id_equipo=eq.id_equipo)");
        $query->execute();
        $asignacion = $query->fetchAll(PDO::FETCH_ASSOC);
        require_once("../content/asignacion/lista.php");
    }

    if ($_POST['accion']==1) {
        $query = $conexion->prepare("INSERT INTO equipo_persona(id_persona,id_equipo) VALUES (?, ?)");
        $resultado = $query->execute(array($_REQUEST["id_personal"],$_REQUEST["id_equipo"]));
        echo $resultado;
    }

    if ($_POST["accion"]==2) {
        $query = $conexion->prepare("UPDATE equipo_persona SET id_persona = ?, id_equipo = ? WHERE id_equipo_persona = ?");
        $resultado = $query->execute(array($_REQUEST["id_personal"],$_REQUEST["id_equipo"],$_REQUEST["asignacion_id"]));
        echo $resultado;
    }

    if ($_POST["accion"]==4) {
        $query=$conexion->prepare("select * from equipo_persona where id_equipo_persona = '{$_REQUEST['id']}' ");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($data);
    }

    if ($_POST["accion"]==5) {
        if ($_REQUEST['tipo']=="anular"){
            $query = $conexion->prepare("UPDATE area SET estado = ?  WHERE id_area = ?");
            $resultado = $query->execute(array("I",$_REQUEST["id"]));
        }
        if ($_REQUEST['tipo']=="restablecer"){
            $query = $conexion->prepare("UPDATE area SET estado = ?  WHERE id_area = ?");
            $resultado = $query->execute(array("A",$_REQUEST["id"]));
        }
        if ($_REQUEST['tipo']=="eliminar"){
            $query = $conexion->prepare("DELETE FROM area WHERE id_area = ?");
            $resultado = $query->execute(array($_REQUEST["id"]));
        }
        echo $resultado;
    }
}
?>
