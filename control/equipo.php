<?php
require '../conexion.php';
if(isset($_POST['accion'])){

    if ($_POST['accion']==0) {
        $query=$conexion->prepare("SELECT eq.*,tp.descripcion as tipo_equipo,es.descripcion as estado 
                                    FROM equipo eq INNER JOIN tipo_equipo tp ON (tp.id_tipo_equipo = eq.id_tipo_equipo)
                                    INNER JOIN estado es ON (es.id_estado = eq.id_estado)
                                    WHERE eq.id_estado = {$_REQUEST['estado']} ");
        $query->execute();
        $equipo = $query->fetchAll(PDO::FETCH_ASSOC);
        require_once("../content/equipo/lista.php");
    }

    //PARA NUEVO
    if ($_POST['accion']==7) {
        $query=$conexion->prepare("SELECT * FROM tipo_equipo WHERE estado = 'A' ");
        $query->execute();
        $tipo_equipo = $query->fetchAll(PDO::FETCH_ASSOC);

        $query3=$conexion->prepare("SELECT * FROM estado WHERE estado = 'A' ");
        $query3->execute();
        $estado = $query3->fetchAll(PDO::FETCH_ASSOC);

        if(isset($_REQUEST['id'])){
            $query5=$conexion->prepare("SELECT * FROM equipo WHERE id_equipo = {$_REQUEST['id']} ");
            $query5->execute();
            $equipo = $query5->fetchAll(PDO::FETCH_ASSOC);
        }

        require_once("../content/equipo/nuevo.php");
    }

    if ($_POST['accion']==1) {
        $query=$conexion->prepare("select * from equipo where ip_equipo = '{$_REQUEST['ip_equipo']}' ");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if (count($data)==0) {
            $query = $conexion->prepare("INSERT INTO equipo(id_tipo_equipo,marca,modelo,usuario,ip_equipo,caracteristicas,id_estado) VALUES (?, ?, ?, ?, ?, ?, ?)");
            $resultado = $query->execute(array($_REQUEST["id_tipo_equipo"],$_REQUEST["marca"],$_REQUEST["modelo"],$_REQUEST["usuario"],$_REQUEST["ip_equipo"],$_REQUEST["caracteristicas"],$_REQUEST["id_estado"]));
            echo $resultado;
        }else{
            echo "2";
        }
    }

    if ($_POST["accion"]==2) {
        $query=$conexion->prepare("select * from equipo where ip_equipo = '{$_REQUEST['ip_equipo']}' ");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if (count($data)==0){
            $query2 = $conexion->prepare("UPDATE equipo SET id_tipo_equipo=?, marca=?, modelo=?, usuario=?, ip_equipo=?, caracteristicas=?, id_estado=? WHERE id_equipo = ?");
            $resultado = $query2->execute(array($_REQUEST["id_tipo_equipo"],$_REQUEST["marca"],$_REQUEST["modelo"],$_REQUEST["usuario"],$_REQUEST["ip_equipo"],$_REQUEST["caracteristicas"],$_REQUEST["id_estado"],$_REQUEST["equipo_id"]));
            echo $resultado;
        } else {
            if (count($data)>=1) {
                $query1=$conexion->prepare("select * from equipo where ip_equipo = '{$_REQUEST['ip_equipo']}' and id_equipo = {$_REQUEST["equipo_id"]}");
                $query1->execute();
                $data1 = $query1->fetchAll(PDO::FETCH_ASSOC);
                if (count($data1)==1){
                    $query2 = $conexion->prepare("UPDATE equipo SET id_tipo_equipo=?, marca=?, modelo=?, usuario=?, ip_equipo=?, caracteristicas=?, id_estado=? WHERE id_equipo = ?");
                    $resultado = $query2->execute(array($_REQUEST["id_tipo_equipo"],$_REQUEST["marca"],$_REQUEST["modelo"],$_REQUEST["usuario"],$_REQUEST["ip_equipo"],$_REQUEST["caracteristicas"],$_REQUEST["id_estado"],$_REQUEST["equipo_id"]));
                    echo $resultado;
                } else {
                    echo "2";
                }
            }else{
                echo "2";
            }
        }
    }

    if ($_POST["accion"]==4) {
        $query=$conexion->prepare("select * from tipo_equipo where id_tipo_equipo = '{$_REQUEST['id']}' ");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($data);
    }

    if ($_POST["accion"]==5) {
        if ($_REQUEST['tipo']=="anular"){
            $query = $conexion->prepare("UPDATE tipo_equipo SET estado = ?  WHERE id_tipo_equipo = ?");
            $resultado = $query->execute(array("I",$_REQUEST["id"]));
        }
        if ($_REQUEST['tipo']=="restablecer"){
            $query = $conexion->prepare("UPDATE tipo_equipo SET estado = ?  WHERE id_tipo_equipo = ?");
            $resultado = $query->execute(array("A",$_REQUEST["id"]));
        }
        if ($_REQUEST['tipo']=="eliminar"){
            $query = $conexion->prepare("DELETE FROM tipo_equipo WHERE id_tipo_equipo = ?");
            $resultado = $query->execute(array($_REQUEST["id"]));
        }
        echo $resultado;
    }
}
?>
