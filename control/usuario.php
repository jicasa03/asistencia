<?php
require '../conexion.php';
if(isset($_POST['accion'])){

    if ($_POST['accion']==0) {
        $query=$conexion->prepare("SELECT * FROM usuario WHERE estado = '{$_REQUEST['estado']}' ");
        $query->execute();
        $usuarios = $query->fetchAll(PDO::FETCH_ASSOC);
        require_once("../content/usuarios/lista.php");
    }

    if ($_POST['accion']==1) {
        $query=$conexion->prepare("select * from usuario where usuario = '{$_REQUEST["usuario"]}' ");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if (count($data)==0) {
            $query1 = $conexion->prepare("INSERT INTO usuario(nombres,usuario,clave,estado) VALUES (?, ?, ?, ?)");
            $resultado = $query1->execute(array($_REQUEST["nombres"],$_REQUEST["usuario"],$_REQUEST["clave"],'A'));
            echo $resultado;
        }else{
            echo "2";
        }
    }

    if ($_POST["accion"]==2) {
        $query=$conexion->prepare("select * from usuario where usuario = '{$_REQUEST["usuario"]}' ");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        if (count($data)==0) {
            $query1 = $conexion->prepare("UPDATE usuario SET nombres = ?, usuario = ?, clave = ? WHERE id_usuario = ?");
            $resultado = $query1->execute(array($_REQUEST["nombres"],$_REQUEST['usuario'],$_REQUEST["clave"],$_REQUEST["usuario_id"]));
            echo $resultado;
        }else{
            if (count($data)>=1) {
                $query=$conexion->prepare("select * from usuario where usuario = '{$_REQUEST["usuario"]}' AND id_usuario = {$_REQUEST['usuario_id']} ");
                $query->execute();
                $data1 = $query->fetchAll(PDO::FETCH_ASSOC);
                if (count($data1)==1){
                    $query1 = $conexion->prepare("UPDATE usuario SET nombres = ?, usuario = ?, clave = ? WHERE id_usuario = ?");
                    $resultado = $query1->execute(array($_REQUEST["nombres"],$_REQUEST['usuario'],$_REQUEST["clave"],$_REQUEST["usuario_id"]));
                    echo $resultado;
                } else {
                    echo "2";
                }
            }else{
                echo "2";
            }
        }
    }

    if ($_POST["accion"]==4) {
        $query=$conexion->prepare("select * from usuario where id_usuario = '{$_REQUEST['id']}' ");
        $query->execute();
        $data = $query->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($data);
    }

    if ($_POST["accion"]==5) {
        if ($_REQUEST['tipo']=="anular"){
            $query = $conexion->prepare("UPDATE usuario SET estado = ?  WHERE id_usuario = ?");
            $resultado = $query->execute(array("I",$_REQUEST["id"]));
        }
        if ($_REQUEST['tipo']=="restablecer"){
            $query = $conexion->prepare("UPDATE usuario SET estado = ?  WHERE id_usuario = ?");
            $resultado = $query->execute(array("A",$_REQUEST["id"]));
        }
        if ($_REQUEST['tipo']=="eliminar"){
            $query = $conexion->prepare("DELETE FROM usuario WHERE id_usuario = ?");
            $resultado = $query->execute(array($_REQUEST["id"]));
        }
        echo $resultado;
    }
}
?>
