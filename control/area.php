<?php
 require '../conexion.php';
    if(isset($_POST['accion'])){

        if ($_POST['accion']==0) {
            $query=$conexion->prepare("SELECT * FROM area WHERE estado = '{$_REQUEST['estado']}' ");
            $query->execute();
            $areas = $query->fetchAll(PDO::FETCH_ASSOC);
            require_once("../content/area/lista.php");
        }

        if ($_POST['accion']==1) {
            $query=$conexion->prepare("select * from area where descripcion = '{$_REQUEST['descripcion']}' ");
            $query->execute();
            $data = $query->fetchAll(PDO::FETCH_ASSOC);
            if (count($data)==0) {
                $query1 = $conexion->prepare("INSERT INTO area(descripcion,estado) VALUES (?, ?)");
                $resultado = $query1->execute(array($_REQUEST["descripcion"],'A'));
                echo $resultado;
            }else{
                echo "2";
            }
        }

        if ($_POST["accion"]==2) {
            $query=$conexion->prepare("select * from area where descripcion = '{$_REQUEST['descripcion']}' ");
            $query->execute();
            $data = $query->fetchAll(PDO::FETCH_ASSOC);
            if (count($data)>=1) {
                echo "2";
            }else{
                $query1 = $conexion->prepare("UPDATE area SET descripcion = ?  WHERE id_area = ?");
                $resultado = $query1->execute(array($_REQUEST["descripcion"],$_REQUEST["area_id"]));
                echo $resultado;
            }
        }

        if ($_POST["accion"]==4) {
            $query=$conexion->prepare("select * from area where id_area = '{$_REQUEST['id']}' ");
            $query->execute();
            $data = $query->fetchAll(PDO::FETCH_ASSOC);
            echo json_encode($data);
        }

        if ($_POST["accion"]==5) {
            if ($_REQUEST['tipo']=="anular"){
                $query = $conexion->prepare("UPDATE area SET estado = ?  WHERE id_area = ?");
                $resultado = $query->execute(array("I",$_REQUEST["id"]));
            }
            if ($_REQUEST['tipo']=="restablecer"){
                $query = $conexion->prepare("UPDATE area SET estado = ?  WHERE id_area = ?");
                $resultado = $query->execute(array("A",$_REQUEST["id"]));
            }
            if ($_REQUEST['tipo']=="eliminar"){
                $query = $conexion->prepare("DELETE FROM area WHERE id_area = ?");
                $resultado = $query->execute(array($_REQUEST["id"]));
            }
            echo $resultado;
        }
    }
?>
