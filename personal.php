<?php include('cabecera.php');
$query1=$conexion->prepare("SELECT * FROM area WHERE estado = 'A' ");
$query1->execute();
$area = $query1->fetchAll(PDO::FETCH_ASSOC);

$query2=$conexion->prepare("SELECT * FROM tipo_persona WHERE estado = 'A' ORDER BY id_tipo_persona DESC");
$query2->execute();
$tipo_persona = $query2->fetchAll(PDO::FETCH_ASSOC);
?>
<div class="header">
    <h3 class="page-header"><b>Lista de Personal</b></h3>
</div>
<div id="page-inner">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading right">
                    <button class="btn btn-success" id="nuevo_personal"><i class="fa fa-plus"></i> NUEVO</button>
                    <button class="btn btn-primary" id="personal_activo"><i class="fa fa-check"></i> ACTIVOS</button>
                    <button class="btn btn-danger" id="personal_inactivo"><i class="fa fa-times"></i> INACTIVOS</button>
                    <a href="reportes/personal.php" class="btn btn-info"><i class="fa fa-file-o"></i> REPORTE</a>
                    <a href="imprimir/personal.php" class="btn btn-default" target="_blank"><i class="fa fa-print"></i> IMPRIMIR</a>
                </div>
                <div class="panel-body">
                    <div id="content"></div>
                </div>
            </div>
            <!--End Advanced Tables -->
        </div>
    </div>
</div>

<!-- MODAL REGISTRAR-->
<div class="modal fade" id="modal_personal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="titulo"><b>Registar Personal</b></h4>
            </div>
            <form class="form-horizontal" id="form_personal">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <input type="hidden" name="personal_id" id="personal_id" value="-1">
                                <label for="inputEmail3" class="col-sm-3 control-label">Nombre</label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" name="nombres" id="nombres" placeholder="Nombres y Apellidos">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Área</label>
                                <div class="col-sm-7">
                                    <select class="form-control" autocomplete="off"  name="id_area" id="id_area">
                                        <option value="">:: SELECCIONE ::</option>
                                        <?php foreach ($area as $value){ ?>
                                                <option value="<?php echo $value['id_area']?>"><?php echo $value['descripcion'];?></option>
                                            <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Jefe de Área</label>
                                <div class="col-sm-7">
                                    <select class="form-control" name="id_tipo_persona" id="id_tipo_persona">
                                        <?php foreach ($tipo_persona as $value){ ?>
                                            <option value="<?php echo $value['id_tipo_persona']?>"><?php echo $value['descripcion'];?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                            
                                <label for="inputEmail3" class="col-sm-3 control-label">Documento</label>
                                <div class="col-sm-7">
                                    <input type="text" maxlength="8" required="true" class="form-control" name="dni" autocomplete="off" id="dni" placeholder="Documento">
                                </div>
                            </div>
                            <!--
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Jefe de Área</label>
                                <div class="col-sm-7">
                                    <div class="radio3 radio-check radio-danger radio-inline">
                                        <input type="radio" id="radio5" name="jefe" value="NO">
                                        <label for="radio5">
                                            NO
                                        </label>
                                    </div>
                                    <div class="radio3 radio-check radio-success radio-inline">
                                        <input type="radio" id="radio6" name="jefe" value="SI">
                                        <label for="radio6">
                                            SI
                                        </label>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
                    <button type="button" class="btn btn-success" id="guardar_personal"><i class="fa fa-save"></i> Guardar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- MODAL CONFIRMACION-->
<div class="modal fade" id="modal_confirmacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header btn-danger">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center" id="titulo"><b><i class="fa fa-warning"></i> Advertencia</b></h4>
            </div>
            <form class="form-horizontal" id="form_area">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="modal-title text-center" id="content_confirmacion"><b></b></h4>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal"><i class="fa fa-times"></i> No, Cerrar!</button>
                    <button type="button" class="btn btn-success" onclick="eliminar_personal()"><i class="fa fa-check"></i> Si, Seguro!</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>
<script>
    $(document).ready(function () {
        $(".mantenimiento").addClass("active-menu");
        $(".mantenimiento_menu").addClass("in");
        $(".personal").addClass("menu-select");

        listado("A");

        $("#personal_activo").click(function(e) {
            listado("A");
        });

        $("#personal_inactivo").click(function(e) {
            listado("I");
        });
    });
</script>
<script src="app/personal.js"></script>
