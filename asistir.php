<?php 

require('conexion.php');

$query2=$conexion->prepare("SELECT * FROM tipo_horario WHERE tipo_horario_estado = 1");
$query2->execute();
$tipo_horario = $query2->fetchAll(PDO::FETCH_ASSOC);















?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta content="" name="description" />
    <meta content="webthemez" name="author" />
    <title>Registro de asistencia</title>
    <!-- Bootstrap Styles-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link href="assets/css/login.css" rel="stylesheet" />
</head>

<body style="background: #f5f5f5;">
    <div class="wrapper fadeInDown">

        <div id="mostrar_error" style="display: none" class="alert alert-danger">
  <strong>Alerta!</strong><label id="texto_error"> Error no existe el DNI ingresado</label>
</div>

   <div id="mostrar_correcto" style="display: none" class="alert alert-success">
  <strong>Excelente!</strong><label id="texto_correcto"> Error no existe el DNI ingresado</label>
</div>
        <div id="formContent">
            <form id="datos" onsubmit="return enviar_asistencia()">
            <!-- Tabs Titles -->
            <h2 class="active">REGISTRAR ASISTENCIA</h2>

            <!-- Icon -->
            <div class="first"> <!-- fadeIn -->
                <img src="assets/inicio.PNG" id="icon" alt="User Icon" />
            </div>

            <!-- Login Form -->
            <form action="" id="form_login" method="post">
                <input type="hidden" name="accion" id="accion" value="1"> 
                <input type="text" class="fadeIn second" maxlength="8" autocomplete="off" name="usuario" id="usuario" required placeholder="Documento de Identidad" autofocus="true">
                <div style="padding-left: 35px;padding-right: 35px; padding-top: 20px;   ">
                <select  class="form-control" name="id_tipo_horario" id="id_tipo_horario" required>
                   <?php foreach ($tipo_horario as $value){ ?>
                                            <option value="<?php echo $value['tipo_horario_id']?>"><?php echo $value['tipo_horario_descripcion'];?></option>
                                        <?php } ?>
                </select>
            </div>
               
            </form>

            <div id="formFooter">
                <label style="color: red;"></label>
            </div>



</form>

        </div>
    </div>
</body>
<script src="assets/js/jquery-1.10.2.js"></script>

<script type="text/javascript">
    
function enviar_asistencia() {
   

   $.post("control/asistencia.php",$("#datos").serialize(),function(response){
    console.log(response);
    if(response["estado"]==1){
            $("#mostrar_error").hide();
        $("#mostrar_correcto").show();

        $("#texto_correcto").text(response["mensaje"]);

    }else{
              $("#mostrar_correcto").hide();
    $("#mostrar_error").show();
      $("#texto_error").text(response["mensaje"]);


    }
   },"json");
     
    return false;
}


</script>